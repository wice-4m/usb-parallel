' Original Authors: forthnutter

{{
    This object adds parallel port interface.

  
}}

OBJ
    parallel: "parallel"

VAR
    byte U30_Shadow         { control every thing }
    byte U4_Shadow          { control profiles }
    byte U5_Shadow          { control for data reading }
    byte U6_Shadow          { control for address control }
    byte flags

CON
    U30A_DISABLE = $47      { 01000111 }
    U30_SEL_U4 = $44
    U30_SEL_U5 = $45
    U30_SEL_U6 = $46
    U30_GLED = %00001000

    U30_DISABLE = 3         { $00000011 }

    U30_MODELATCH = 2       { $00000100 }
    U30_GLEDL = 3
    U30_MODE = 5         { bit 5 Low port A High port B }
    U30_PALCLK = 6

    FLAG_ACTIVE = 0

    TEST_BYTE1 = $5A
    TEST_BYTE2 = $A5
    MEM_SIZE = 262144

    U4_SELECT = 0
    U5_SELECT = 1
    U6_SELECT = 2
    CLR_SELECT = 3
    U4S_SELECT = $80
    U5S_SELECT = $81
    U6S_SELECT = $82
    CLS_SELECT = $83
    

PUB Status : retval
    retval := flags


PRI Enable_Strobe(mode)
    CASE mode
        0:
            parallel.LF_Byte ($CB)
            parallel.Read_Data_Wait
            parallel.LF_Byte ($CF)
            parallel.Read_Data_Wait
            parallel.LF_Byte ($47)
            parallel.Read_Data_Wait
        1:
            parallel.LF_Byte ($DB)
            parallel.Read_Data_Wait
            parallel.LF_Byte ($DF)
            parallel.Read_Data_Wait
            parallel.LF_Byte ($47)
            parallel.Read_Data_Wait

PRI Inc_Address
    parallel.Init_Line  { increment Address }
    result := parallel.Read_Data_Wait

PUB Write_Memory(data , inc)
    if(active)
        'Strobe_Write(U5S_SELECT,$C7)
        parallel.Write_Byte (data)
        parallel.Read_Data_Wait
        result := Fast_Read_Byte(inc)
   

PUB Reset_Address
    if(active)
        parallel.Reset_Address
        result := parallel.Read_Data_Wait
        

PRI Read_Status
    parallel.Read_Status
    result := parallel.Read_Data_Wait
    

PRI active
    result := (flags & FLAG_ACTIVE) == FLAG_ACTIVE

PRI wice_active_flag(flag)
    if(flag)
        flags |= |<FLAG_ACTIVE
    else
        flags &= !|<FLAG_ACTIVE

PRI Fill_Memory(fill)
    if(active)
        repeat MEM_SIZE
            Write_Memory(fill,1)


PUB Read_Address
    result := 0
    parallel.Read_Address
    result := parallel.Read_Data_Wait


PUB Fast_Read_Byte (inc)
    result := 0
    if(active)
        parallel.read_byte_fast(inc)
        result := parallel.Read_Data_Wait
        
        
PUB Fast_Read_Word (inc)
    result := 0
    if(active)
        parallel.read_word_fast (inc)
        result := parallel.Read_Data_Wait

            
PUB GLED_On
    U30_Shadow |= U30_GLED
    'LF_Write(U30_Shadow)

PUB GLED_Off
    U30_Shadow &= !U30_GLED
    'LF_Write(U30_Shadow)



PUB Test_Memory | ra, rb, rd
    result := false
    if(active)
        ra := false
        rb := false
        'Write_Memory(TEST_BYTE1,0)
        'rd := Read_Memory(0)
        if(rd == TEST_BYTE1)
            ra := true
        parallel.Init_Line   {increment Address}
        'Write_Memory(TEST_BYTE2,0)
        'rd := Read_Memory(0)
        if(rd == TEST_BYTE2)
            rb := true
        result := ra == rb



PUB Open(data)
    Enable_Strobe(data)
    wice_active_flag(true)

PUB Close
    parallel.LF_Byte ($4B)
    parallel.Read_Data_Wait
    parallel.LF_Byte ($4F)
    parallel.Read_Data_Wait
    wice_active_flag(false)

        
PRI LF_Disable
    U30_Shadow := |<U30_PALCLK | |<U30_MODELATCH | U30_DISABLE
    parallel.LF_Byte (U30_Shadow)
    parallel.Read_Data_Wait


PRI Strobe_Write(select,value)
    if(active)
        parallel.Strobe_Write (select,value)
        parallel.Read_Data_Wait

' this should return 32bit value of shadow register
' U30 U4 U5 U6            
PUB Read_Shadow
    parallel.read_shadow
    result := parallel.Read_Data_Wait
    
PUB Start | rdata
    result := false
    parallel.Start



'    parallel.LF_Byte (0)
'    parallel.Read_Data_Wait
'    parallel.LF_Byte (1)
'    parallel.Read_Data_Wait
'    parallel.LF_Byte (2)
'    parallel.Read_Data_Wait
'    parallel.LF_Word ($aa55)
'    parallel.Read_Data_Wait
'    parallel.LF_Long ($aa55a55a)
'    parallel.Read_Data_Wait
'    parallel.LFStrobe_Long ($05060708)
'    parallel.Read_Data_Wait
    
    'parallel.LF_Byte (3)   
    LF_Disable
    Open(0)

    Strobe_Write(U4_SELECT,$FF) { Initilise U4 Latch and shadow }
    Strobe_Write(U5_SELECT,$C7) { Initilise U5 Latch and Shadow }
    Strobe_Write(U6_SELECT,$11) { initilise U6 Shadow mem }
    Strobe_Write(U6S_SELECT, 1) { now set U6 to Default }


'    Read_Memory
'    parallel.Init_Line  { increment Address }
'    Reset_Address

'    if(Test_Memory)
'        result := true

    'Reset_Address

'    Fill_Memory($AA)

    Close


