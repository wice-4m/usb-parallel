'     U2_OE = 17
con
	U2_OE = 17
'     U2_DIR = 23
	U2_DIR = 23
'     U3_DIR = 18
	U3_DIR = 18
'     U3_STROBE = 8
	U3_STROBE = 8
'     U3_LINEFEED = 9
	U3_LINEFEED = 9
'     U4_DIR = 19
	U4_DIR = 19
'     U4_INITS = 10
	U4_INITS = 10
'     U4_SELIN = 11
	U4_SELIN = 11
'     U5_DIR = 20
	U5_DIR = 20
'     U5_ACK = 12
	U5_ACK = 12
'     U5_BUSY = 13
	U5_BUSY = 13
'     U6_DIR = 21
	U6_DIR = 21
'     U6_PEND = 14
	U6_PEND = 14
'     U6_SELECT = 15
	U6_SELECT = 15
'     U7_DIR = 22
	U7_DIR = 22
'     U7_ERROR = 16
	U7_ERROR = 16
' 
'     RD_ADVALID = 0
	RD_ADVALID = 0
'     WR_ADVALID = 4
	WR_ADVALID = 4
'     RD_ADLDATA = 8
	RD_ADLDATA = 8
'     WR_ADLDATA = 12
	WR_ADLDATA = 12
pub main
  coginit(0, @entry, 0)
dat
	org	0
entry
	mov	arg01, par wz
	locknew	__lockreg
	wrlong	__lockreg, ptr___lockreg_
 if_ne	jmp	#spininit
	mov	pc, $+2
	call	#LMM_CALL_FROM_COG
	long	@@@_OE_Enable
cogexit
	cogid	arg01
	cogstop	arg01
spininit
	mov	sp, arg01
	rdlong	objptr, sp
	add	sp, #4
	rdlong	pc, sp
	wrlong	ptr_hubexit_, sp
	add	sp, #4
	rdlong	arg01, sp
	add	sp, #4
	rdlong	arg02, sp
	add	sp, #4
	rdlong	arg03, sp
	add	sp, #4
	rdlong	arg04, sp
	sub	sp, #12
	jmp	#LMM_LOOP
LMM_LOOP
    rdlong LMM_i1, pc
    add    pc, #4
LMM_i1
    nop
    rdlong LMM_i2, pc
    add    pc, #4
LMM_i2
    nop
    rdlong LMM_i3, pc
    add    pc, #4
LMM_i3
    nop
    rdlong LMM_i4, pc
    add    pc, #4
LMM_i4
    nop
    rdlong LMM_i5, pc
    add    pc, #4
LMM_i5
    nop
    rdlong LMM_i6, pc
    add    pc, #4
LMM_i6
    nop
    rdlong LMM_i7, pc
    add    pc, #4
LMM_i7
    nop
    rdlong LMM_i8, pc
    add    pc, #4
LMM_i8
    nop
LMM_jmptop
    jmp    #LMM_LOOP
pc
    long @@@hubentry
lr
    long 0
hubretptr
    long @@@hub_ret_to_cog
LMM_NEW_PC
    long   0
    ' fall through
LMM_CALL
    rdlong LMM_NEW_PC, pc
    add    pc, #4
LMM_CALL_PTR
    wrlong pc, sp
    add    sp, #4
LMM_JUMP_PTR
    mov    pc, LMM_NEW_PC
    jmp    #LMM_LOOP
LMM_JUMP
    rdlong pc, pc
    jmp    #LMM_LOOP
LMM_RET
    sub    sp, #4
    rdlong pc, sp
    jmp    #LMM_LOOP
LMM_CALL_FROM_COG
    wrlong  hubretptr, sp
    add     sp, #4
    jmp  #LMM_LOOP
LMM_CALL_FROM_COG_ret
    ret
    
LMM_CALL_ret
LMM_CALL_PTR_ret
LMM_JUMP_ret
LMM_JUMP_PTR_ret
LMM_RET_ret
LMM_RA
    long	0
    
LMM_FCACHE_LOAD
    rdlong FCOUNT_, pc
    add    pc, #4
    mov    ADDR_, pc
    sub    LMM_ADDR_, pc
    tjz    LMM_ADDR_, #a_fcachegoaddpc
    movd   a_fcacheldlp, #LMM_FCACHE_START
    shr    FCOUNT_, #2
a_fcacheldlp
    rdlong 0-0, pc
    add    pc, #4
    add    a_fcacheldlp,inc_dest1
    djnz   FCOUNT_,#a_fcacheldlp
    '' add in a JMP back out of LMM
    ror    a_fcacheldlp, #9
    movd   a_fcachecopyjmp, a_fcacheldlp
    rol    a_fcacheldlp, #9
a_fcachecopyjmp
    mov    0-0, LMM_jmptop
a_fcachego
    mov    LMM_ADDR_, ADDR_
    jmpret LMM_RETREG,#LMM_FCACHE_START
a_fcachegoaddpc
    add    pc, FCOUNT_
    jmp    #a_fcachego
LMM_FCACHE_LOAD_ret
    ret
inc_dest1
    long (1<<9)
LMM_LEAVE_CODE
    jmp LMM_RETREG
LMM_ADDR_
    long 0
ADDR_
    long 0
FCOUNT_
    long 0

__lockreg
	long	0
imm_131072_
	long	131072
objptr
	long	@@@objmem
ptr___lockreg_
	long	@@@__lockreg
ptr_hubexit_
	long	@@@hubexit
sp
	long	@@@stackspace
COG_BSS_START
	fit	496
hub_ret_to_cog
	jmp	#LMM_CALL_FROM_COG_ret
hubentry

' 
' 
' PRI OE_Enable
_OE_Enable
'     OUTA[U2_OE]~
	andn	outa, imm_131072_
_OE_Enable_ret
	call	#LMM_RET
hubexit
	jmp	#cogexit
objmem
	long	0[4]
stackspace
	long	0[1]
	org	COG_BSS_START
arg01
	res	1
arg02
	res	1
arg03
	res	1
arg04
	res	1
local01
	res	1
LMM_RETREG
	res	1
LMM_FCACHE_START
	res	97
LMM_FCACHE_END
	fit	496
