CON

    _clkmode = xtal1 + pll16x
    _xinfreq = 5_000_000
    MAX_LEN = 64
    Version = $00000011
    
    RAW_NOP =           0       'like it says no operation
    RAW_STATUS =        1       'get status what port is open
    RAW_ARESET =        2       'Reset Address Counter PASM
    RAW_OPEN =          3       'open port A or B 0 or 1
    RAW_CLOSE =         4       'close all ports
    RAW_READ8 =         7       'read 8 bits via PASM
    RAW_READ8INC =      8       'read 8 bits increment address PASM
    RAW_READ16 =        9       'read 16 bits
    RAW_READ16INC =     10      'read 16 bits from memory auto incrememt address PASM
    RAW_READ32 =        11      'read 32 bits
    RAW_READ32INC =     12      'read 32 bits from memory address increment PASM
    RAW_WRITE8 =        13      'write 8 bits to memory PASM
    RAW_WRITE8INC =     14      'write 8 bits to memory with address increment PASM
    RAW_WRITE16 =       15      'write 16 bits to memory
    RAW_WRITE16INC =    16      'write 16 bits to memory with address inc PASM
    RAW_WRITE32 =       17      'write 32 bits
    RAW_WRITE32INC =    18      'write 32 bits to memory with address inc PASM
    RAW_READASHADOW =   19      'read the address counter shadow
    RAW_READUSHADOW =   20      'read Strobe latch shadow
    

    
    ' ASC are ascii character for testing
    ' Delete this if we are ready for release
    ASC_NOP =           "0"
    ASC_STATUS =        "1"
    ASC_ARESET =        "2"
    ASC_OPEN =          "3"
    ASC_CLOSE =         "4"
    ASC_READ8 =         "5"
    ASC_READ8INC =      "6"
    ASC_READ16 =        "7"
    ASC_READ16INC =     "8"
    ASC_READ32 =        "9"
    ASC_READ32INC =     "A"
    ASC_WRITE8 =        "B"
    ASC_WRITE8INC =     "C"
    ASC_WRITE16 =       "D"
    ASC_WRITE16INC =    "E"
    ASC_WRITE32 =       "F"
    ASC_WRITE32INC =    "G"
    ASC_READASHADOW =   "H"
    ASC_READUSHADOW =   "I"

    
    
    
VAR
    byte  pstr[MAX_LEN]

OBJ

    term : "Parallax Serial Terminal"   ' "com.serial.terminal"
    bug  : "PC_Debug"                                        ' 
    wice : "wice-4m"
    num  : "string.integer"
    str  : "string"
   

PUB Main | data
    term.Start(115200)
    'bug.startx(24,25,115200)

    'bug.Str(string("Version "))
    'bug.Str(String_Version)
    'bug.crlf
    'bug.Str (string("Baud = 115200"))
    'bug.crlf
    
    wice.Start
    'bug.Str (string("Wice Started"))
    'bug.crlf
    
    repeat
        
        case term.CharIn
            RAW_NOP : 'return 0 more of communication test
                term.Char (0)

            RAW_STATUS : 'read the status flag
                data := wice.Status
                term.Char (data)

            RAW_ARESET : 'reset address counter
                data := wice.Reset_Address
                term.Char (data.byte[0])
                term.Char (data.byte[1])
                term.Char (data.byte[2])
                term.Char (data.byte[3])

            RAW_READ8 : 'read memory
                data := wice.Fast_Read_Byte (0)
                'data := wice.Read_Memory(0)
                term.Char (data)

            RAW_READ8INC : 'read memory and increment address
                data := wice.Fast_Read_Byte (1)
                term.Char (data)

            5 : ' read shadow address
                data := wice.Read_Address
                SendAddress(data)

            6 : ' open port for reading and writing
                data := term.CharIn
                data &= 1
                wice.Open (data)
                data := wice.Status
                term.Char (data)

            7 : ' Close any port
                wice.Close
                data := wice.Status
                term.Char (data)

            8 : ' write to wice memory
                data := term.CharIn
                data := wice.Write_Memory (data , 0)
                term.Char (data)

            9: ' write to wice memory and increment address
                data := term.CharIn
                data := wice.Write_Memory (data , 1)
                term.Char (data)

            10: ' 0A Read the U30 Shadow 
'                data := wice.LF_Read
                term.Char (data)

            11: ' 0B Read the U4 shadow
                'data := wice.Read_U4_Shadow
                term.Char (data)

            12: ' 0C Read the U5 shadow
                'data := wice.Read_U5_Shadow
                term.Char (data)

            13: ' 0D Read the U6 shadow
                'data := wice.Read_U6_Shadow
                term.Char (data)

            14: ' 0E Write value to U30 shadow latch
                data := term.CharIn
'                data := wice.Write_U30_Shadow (data)
                term.Char(data)

            15: ' 0F Write value to U4 Shadow Latch
                data := term.CharIn
 '               data := wice.Write_U4_Shadow (data)
                term.Char(data)

            16: ' 10 Write value to U5 Shadow Latch
                data := term.CharIn
  '              data := wice.Write_U5_Shadow (data)
                term.Char(data)

            17: ' 11 Write value to U6 Shadow Latch
                data := term.CharIn
   '             data := wice.Write_U6_Shadow (data)
                term.Char(data)

            18: ' 12 get the firmware version
                


            ASC_NOP: 'repeat code 0
                term.Hex (0, 2)

            ASC_STATUS: ' 1 read the status flags
                data := wice.Status
                term.Hex (data, 2)
                
            ASC_ARESET: ' 2 reset address
                data := wice.Reset_Address
                term.Hex (data, 8)

            ASC_OPEN : ' 3 open port for reading and writing
                data := term.CharIn
                data &= 1
                wice.Open (data)
                data := wice.Status
                term.Hex (data, 2)

            ASC_CLOSE : ' 4 close any port
                wice.Close
                data := wice.Status
                term.Hex (data, 2)

            ASC_READ8 : ' 5 read memory
                data := wice.Fast_Read_Byte (0)
                term.Hex (data, 2)

            ASC_READ8INC : ' 6 read memory increment address
                data := wice.Fast_Read_Byte (1)
                term.Hex (data, 2)
                
            ASC_READ16 : ' 7
                data := wice.Fast_Read_Word (0)
                term.Hex (data,4)
                
            ASC_READ16INC : ' 8 
                data := wice.Fast_Read_Word (1)
                term.Hex (data,4)

            ASC_READ32 :    ' 9
                data := wice.Fast_Read_Word (0)
                term.Hex (data,8)
                
            ASC_READ32INC : ' A
            ASC_WRITE8 :    ' B
                term.StrInMax (@pstr, 2)
                data := num.StrToBase(@pstr,16)
                data := wice.Write_Memory (data , 0)
                term.Hex (data, 2)
                
            ASC_WRITE8INC : ' C
                term.StrInMax (@pstr,2)
                data := num.StrToBase (@pstr,16)
                data := wice.Write_Memory (data , 1)
                term.Hex (data,2)
                
            ASC_WRITE16 :   ' D
            ASC_WRITE16INC : ' E
            ASC_WRITE32 :   ' F
            ASC_WRITE32INC :    'G
            
            ASC_READASHADOW :   ' H
                data := wice.Read_Address
                term.Hex (data, 8)
                 
            ASC_READUSHADOW :   ' I
                data := wice.Read_Shadow
                term.Hex (data,8)

                
            OTHER:
                'term.Char (di)
               
PRI SendAddress(data) | Index
{{ get two bytes from serial to make word }}
 
    repeat Index from 3 to 0
        term.Char (data.byte[Index])


PRI String_Version | ver, size1
    ver := Version
    
    str.Copy(@pstr,num.Dec(ver.byte[3]))
    str.Append(@pstr,string("."))
    str.Append(@pstr,num.Dec (ver.byte[2]))
    str.Append(@pstr,string("."))
    str.Append(@pstr,num.Dec(ver.byte[1]))
    str.Append(@pstr,string("."))
    str.Append(@pstr,num.Dec(ver.byte[0]))
    return @pstr
