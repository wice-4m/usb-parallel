' Original Authors: forthnutter

{{
    This object adds parallel port interface.

  
}}
CON
    U3_DIR = 18
    U3_STROBE = 8
    U3_LINEFEED = 9
    U4_DIR = 19
    U4_INITS = 10
    U4_SELIN = 11
    U5_DIR = 20
    U5_ACK = 12
    U5_BUSY = 13
    U6_DIR = 21
    U6_PEND = 14
    U6_SELECT = 15
    U7_DIR = 22
    U7_ERROR = 16
    
    U6_BIT_ARESET = %00010000

    RD_ADVALID = 0
    WR_ADVALID = 4
    RD_ADLDATA = 8
    WR_ADLDATA = 12

    PASM_NOP = 0
    PASM_LF1BYTE = 1
    PASM_ST1BYTE = 2
    PASM_STSELBYTE = 3
    PASM_READ4BITS = 4
    PASM_READ8BITS = 5    ' fasbyte read
    PASM_READ16BITS = 6
    PASM_READ32BITS = 7
    PASM_WRITE8BITS = 8
    PASM_WRITE16BITS = 9
    PASM_WRITE32BITS = 10
    PASM_INCADR = 11
    PASM_READADR = 12
    PASM_RESETADR = 13
    PASM_READSHADOW = 14

    
VAR
    long rd_status
    long wr_status
    long rd_value
    long wr_value

OBJ
    bug  : "PC_Debug"
    

PRI Send_Command(code,value,str)
    repeat while(rd_status & |<31)
        bug.out (str)
        bug.hex (rd_status,32)
        bug.crlf
    rd_value := value
    rd_status := code
    rd_status |= |<31
    bug.out (str)
    bug.hex (rd_status,32)
    bug.out (" ")
    bug.hex (rd_value,32)
    bug.crlf
 
PUB Read_Data_Wait
    repeat until (wr_status & |<31)
        bug.out ("R")
        bug.hex (wr_status, 32)
        bug.crlf
    

    result := wr_value   
    wr_status &= !|<31
    bug.out ("R")
    bug.hex (wr_status, 32)
    bug.out (" ")
    bug.hex (wr_value,32)
    bug.crlf 
    
' sometimes we need to reset the address counter        
PUB Reset_Address
    Send_Command(PASM_RESETADR,0,"U")
    
' get the value of address counter shadow value   
PUB Read_Address
    Send_Command(PASM_READADR,0,"C")

' this command will increment the address counter
PUB Init_Line
    Send_Command(PASM_INCADR,1,"I")

' write to memory
PUB Write_Byte(value)
    Send_Command(PASM_WRITE8BITS,value,"W")

' latch a value to the Line feed register the Control Register
PUB LF_Byte(value)
    Send_Command(PASM_LF1BYTE,value,"B")
    
' send byte to strobe register the address of strobe register is 
' controlled by Control register    
PUB Strobe_Byte(value)
    Send_Command(PASM_ST1BYTE,value,"S")

' read the four bit status register     
PUB Read_Status
    Send_Command(PASM_READ4BITS,0,"A")

' read one byte with address increment
PUB read_byte_fast(inc)
    Send_Command(PASM_READ8BITS,inc,"T")

' read two bytes with inc at end
PUB read_word_fast(inc)
    Send_Command(PASM_READ16BITS,inc,"W")

' read the shadow register of strobe latches
' U30 U4 U5 U6
PUB read_shadow
    Send_Command(PASM_READSHADOW,0,"V")
    
' Write a value to strobe register
PUB Strobe_Write(select,value) | val
    val := select << 8 | value          ' the select is in upper 8 bits value in lower
    Send_Command(PASM_STSELBYTE,val,"S")
    
    
PUB Start:okay

    bug.startx(24,25,115200)
    rd_status := 0               ' make sure all valid bit are cleared
    wr_status := 0               ' clear all bits to indicate data is not valid
 
    COGNEW(@entry,@rd_status)
    
    
    return okay
    
DAT
                org 0


entry
                mov         t1,par              ' get the adress of the structure that contain all info
                xor         $,$  NR,WC          ' clear carry flag
                muxc        outa, INIT_OUT_L    ' make sure all output that start low state are low
                muxnc       outa, INIT_OUT_H    ' this set all pins to high that need to be high
                muxc        dira, INIT_DIR_IN   ' all dir input pin set to low
                muxnc       dira, INIT_DIR_OUT  ' this make port as output 
    
' Basically we use Data in struct to tell cog we have something to do
' and Data out lock to send messages back
 
theloop
                call        #readcmd            ' get the command we need to process
                                                ' if get here we have got a command
                mov         wrstatus,rdstatus   ' copy status to send back
                mov         wrvalue,rdvalue     ' copy value to send

                cmp         rdstatus,#PASM_NOP WZ      ' do nothing
        IF_Z    call        #do_nothing
                cmp         rdstatus,#PASM_LF1BYTE WZ  ' send 1 byte line feed
        IF_Z    call        #do_linefeed        ' 
                cmp         rdstatus,#PASM_ST1BYTE WZ  ' send 1 byte strobe
        IF_Z    call        #do_strobe
                cmp         rdstatus,#PASM_STSELBYTE    WZ  ' select strobe then stobe latch
        IF_Z    call        #do_sel_strobe                                                            ' 
                cmp         rdstatus,#PASM_READ4BITS WZ ' Read 4 bit status
        IF_Z    call        #do_read_status
                cmp         rdstatus,#PASM_READ8BITS  WZ
        IF_Z    call        #do_read_byte       ' test a byte read function
                cmp         rdstatus,#PASM_READ16BITS WZ
        IF_Z    call        #do_read_word       ' do 16 bit read return 2 bytes
                cmp         rdstatus,#PASM_WRITE8BITS WZ ' write data to ram
        IF_Z    call        #do_write_byte      ' write
                cmp         rdstatus,#PASM_INCADR   WZ
        IF_Z    call        #do_ac_inc          ' address counter increment
                cmp         rdstatus,#PASM_READADR WZ
        IF_Z    call        #do_read_address
                cmp         rdstatus,#PASM_RESETADR WZ
        IF_Z    call        #do_reset_address
                cmp         rdstatus,#PASM_READSHADOW   WZ
        IF_Z    call        #do_read_shadow
        
                call        #sendcmd            ' send back the command and value
                jmp         #theloop



' select out put strobe then latch to data to select strobe
do_sel_strobe
                mov         data_parameter,rdvalue      ' save value read in
                shr         data_parameter,#8           ' first 8 bit is data bits 8 and 9
                and         data_parameter,#%00000011   ' we only the first 2 bits
                mov         t1,u30_shadow               ' get U30
                andn        t1,#%00000011               ' clear the 2 bits
                or          data_parameter,t1           ' param has the value we vant
                call        #data_linefeed              ' send it out now
                mov         data_parameter,rdvalue      ' get input data
                and         data_parameter,#$FF         ' mask off the stuff we dont need
                call        #data_strobe                ' output strobe value
                mov         data_parameter,u30_shadow   ' now we disable sel
                or          data_parameter,#%00000011   ' this should disable select
                call        #data_linefeed              ' output that
do_sel_strobe_ret
                ret
                
' read shadow memory and return it back
do_read_shadow
                mov         wrvalue,#0                  ' clear the return value
                mov         data_parameter,u30_shadow   ' get u30 shadow
                and         data_parameter,#$FF         ' mask off thtop bits
                or          wrvalue,data_parameter      ' put the the value
                shl         wrvalue,#8                  ' move U30 it needs to get to upper
                mov         data_parameter,u4_shadow    ' now get U4 shadow
                and         data_parameter,#$FF         ' mask off the top 8 bits
                or          wrvalue,data_parameter      ' merge U30 and U4 together
                shl         wrvalue,#8                  ' make space for U5
                mov         data_parameter,u5_shadow    ' get U5 so we add it
                and         data_parameter,#$FF         ' keep only the lower 8 bits
                or          wrvalue,data_parameter      ' merge in U5
                shl         wrvalue,#8                  ' make room for U6
                mov         data_parameter,u6_shadow    ' U6 is now foucus
                and         data_parameter,#$FF         ' mask everything else
                or          wrvalue,data_parameter      ' merge U6 in
do_read_shadow_ret
                ret
                
' lets reset the address
do_reset_address
                mov         data_parameter,u30_shadow   ' $46 for U6 strobe latch 2 active
                andn        data_parameter,#3           ' clear bits 0 and 1
                or          data_parameter,#2
                call        #data_linefeed              ' now LF Latch with data
                mov         data_parameter,u6_shadow    ' get data from u6
                or          data_parameter,#U6_BIT_ARESET   ' bit 4 set
                call        #data_strobe                ' strobe the upper nible data
                andn        data_parameter,#U6_BIT_ARESET   ' now clear it
                call        #data_strobe                ' strobe it out again
                mov         data_parameter,u30_shadow
                or          data_parameter,#3           ' this should disable all latches
                call        #data_linefeed
                mov         address_count,#0            ' address now 0
                mov         wrvalue,#0                  ' 0 is also returned
do_reset_address_ret
                ret


' Do read of the shadow address
' because we shadow the counting of address here
do_read_address
                mov         wrvalue,address_count
do_read_address_ret
                ret


' read a byte from RAM
' data_parameter returns with nibble
' slight hardware design error
' need to read status bit seperately 
' cannot read data in directly
' bit need to be rearanged to get correct data
' this does slow down read operation a bit more than expected
read_sdata
                xor         t2,t2           ' this should clear t2
                mov         t1,ina          ' readin
                and         t1,INPUT_DATA   ' mask the bit we don't need
                and         t1,U6_SELECT_PIN  WC,NR     ' find out if sect pin is set
                muxc        t2,#%0001                   ' if so set bit 0
                and         t1,U6_PEND_PIN    WC,NR     ' find out if pend is set
                muxc        t2,#%0010                   ' if so set bit 1
                and         t1,U5_ACK_PIN     WC,NR     ' is ack set 
                muxc        t2,#%0100                   ' set bit 2
                and         t1,U5_BUSY_PIN    WC,NR     ' doe we have busy
                muxc        t2,#%1000                   ' then bit 4 
                mov         data_parameter,t2       ' the return data
read_sdata_ret
                ret

read_upper
                mov         data_parameter,u5_shadow
                andn        data_parameter,#%11000001
                or          data_parameter,#%01000000
                call        #data_strobe
                call        #read_sdata
read_upper_ret
                ret 
                
read_lower
                mov         data_parameter,u5_shadow
                andn        data_parameter,#%11000001
                or          data_parameter,#%10000000
                call        #data_strobe
                call        #read_sdata
read_lower_ret
                ret
                
                
' turn the four bits into byte
' data_parameter returns the eight bit value 
read_byte
                mov         data_parameter,u30_shadow   ' get shadow
                andn        data_parameter,#%00000011   ' mask out latch address
                or          data_parameter,#%00000001   ' $45 set strobe latch 1 active
                call        #data_linefeed              ' now LF Latch with data
                call        #read_upper                 ' this reads upper nible
                mov         wrvalue,data_parameter      ' save data to wrvalue send back
                shl         wrvalue,#4                  ' shift 4 bits to the upper nibble
                call        #read_lower                 ' read in the lower nibble
                or          wrvalue,data_parameter      ' set the lower bits of return 
                mov         data_parameter,#%11000111   '$C7 disable read latch
                call        #data_strobe                ' data to disable buffers
                mov         data_parameter,u30_shadow   ' get shadow ram
                or          data_parameter,#%00000011   ' $47 disable control
                call        #data_linefeed              ' LF latch set to default
read_byte_ret  ret

' read byte then see if we need to increment the address couter
do_read_byte
                call        #read_byte                  ' read the byte 
                mov         data_parameter,rdvalue      ' rdvalue has the number to inc
                call        #address_inc                ' do increment address
do_read_byte_ret
                ret

' turn the four bits into word auto increments address counter
' data_parameter returns the 16 bit value 
read_word
                mov         data_parameter,#%01000101   '$45 set strobe latch 1 active
                call        #data_linefeed              ' now LF Latch with data
                mov         data_parameter,#%01000110   ' put $46 into strobe latch
                call        #data_strobe                ' strobe the upper nible data
                call        #read_sdata                 ' read 4 bit status
                mov         wrvalue,data_parameter      ' save data to wrvalue send back
                shl         wrvalue,#4                  ' shift 4 bits to the upper nibble
                mov         data_parameter,#%10000110   '$86 code for lower nible select
                call        #data_strobe                ' now change lower nibble
                call        #read_sdata                 ' read data
                or          wrvalue,data_parameter      ' set the lower bits of return
                shl         wrvalue,#4                  ' shift 4 for the next 4 bits
                call        #Init_Toggle                ' inc address counter
                mov         data_parameter,#%01000110   ' go back and select upper nibble
                call        #data_strobe                ' set strobe latch
                call        #read_sdata                 ' get the status
                or          wrvalue,data_parameter      ' buld up the word
                shl         wrvalue,#4                  ' shift 4 bit read for the next data
                mov         data_parameter,#%10000110   ' set the lower status for reading
                call        #data_strobe                ' strobe data into latch
                call        #read_sdata                 ' read the status
                or          wrvalue,data_parameter      ' save the 4bit
                mov         data_parameter,#%11000111   '$C7 disable read latch
                call        #data_strobe                ' data to disable buffers
                call        #Init_Toggle                ' let go to next address
                mov         data_parameter,#%01000111   '$47 disable control
                call        #data_linefeed              ' LF latch set to default
read_word_ret  ret


' read word
' the address counter will be inc twice
do_read_word
                call        #read_word                  ' read the word into wrvalue
do_read_word_ret
                ret

' the parallel Init signal increment an address counter
do_ac_inc       
                call        #Init_Toggle        ' increment hardware count address
do_ac_inc_ret   ret



do_write_byte
                mov         p1,rdvalue
do_write_sub
                mov         t2,outa
                andn        t2,U2_DATA_PIN
                mov         t1,p1
                and         t1,U2_DATA_PIN
                or          t2,t1
                mov         outa,t2
                call        #Selin
                
do_write_byte_ret
do_write_sub_ret
                ret

 
                
' nothing happening here
do_nothing
                nop
                xor         $,$     NR, WC
                muxnc       outa,LED_P26
do_nothing_ret  ret

' do the line feed service
' loads data on bus
' and then toggle linefeed
' P1 has the value to send
do_linefeed
                mov         data_parameter,rdvalue
                
                call        #data_linefeed
                mov         wrvalue,u30_shadow
do_linefeed_ret
                ret


' do the strobe with data
' loads data on bus
' toggle strobe signal
do_strobe
                mov         data_parameter,rdvalue  ' strobe uses p1 as parameter
                call        #data_strobe            ' strobe the data to the latch
do_strobe_ret   ret


' read a four bite status
' return the value into wrvalue to send back to Hub memory
do_read_status
                call        #read_sdata             ' return 4 bit status
                mov         wrvalue,data_parameter  ' the return data
do_read_status_ret
                ret

' Routine to put data onto data bus ready for latching
' data_parameter holds the 8 bit value
data_out
                mov         t2,outa                 ' get outa status
                andn        t2,U2_DATA_PIN          ' data line cleared
                mov         t1,data_parameter       ' get data to send
                and         t1,U2_DATA_PIN          ' mask 7..0
                or          t2,t1                   ' send data
                mov         outa,t2                 ' return outa
data_out_ret    ret


' work out wich shadow ram gets the data
strobe_shadow
                mov         t2,u30_shadow           ' bits 0 and 1 has latch address
                and         t2,#3                   ' mask off the rest
                cmp         t2,#0   WZ
        IF_Z    mov         u4_shadow,data_parameter
                cmp         t2,#1   WZ
        IF_Z    mov         u5_shadow,data_parameter
                cmp         t2,#2   WZ
        IF_Z    mov         u6_shadow,data_parameter
strobe_shadow_ret
                ret
' routine data out signal strobe
data_strobe
                call        #data_out               ' put data on buss
                call        #strobe_shadow          ' copy data into shadow memory
                call        #Strobe                 ' toggle strobe to latch data
data_strobe_ret
                ret

' routine data out then linfeed
data_linefeed
                call        #data_out                   ' data parameter has data
                mov         u30_shadow,data_parameter   ' save parameter in shadow
                call        #LineFeed                   ' toggle LF
data_linefeed_ret
                ret

' wait for byte read to become valid
' returns with rdcode and rddata to work with
readcmd
                mov         t1,par                  ' make we are lookong at the parameter
                rdlong      rdstatus,t1             ' read in the status
                test        rdstatus,VALID_BIT  WZ  ' test the valid bit
        IF_Z    jmp         #readcmd                ' loop back to update status
                add         t1,#RD_ADLDATA          ' point to rdcode
                rdlong      rdvalue,t1              ' get code data
                andn        rdstatus,VALID_BIT      ' clear that bit
                mov         t1,par                  ' need to get that address back
                wrlong      rdstatus,t1             ' the hub now knows we have read data   
readcmd_ret     ret

' wait for write lock to clear
' send the wrcode and wrdata back to the hub
sendcmd
                mov         t1,par                  ' get the parameter again
                add         t1,#WR_ADVALID          ' add the hub addres of write status
                rdlong      valid,t1                ' must test to see data has been read
                test        valid,VALID_BIT     WZ  ' test that the valid bit is still set
        IF_NZ   jmp         #sendcmd                ' if this bit set we need to wait util cleared
                or          outa,LED_P27
                or          wrstatus,VALID_BIT      ' make sure valid bit is set 
                mov         t1,par                  ' lets grab the parameter
                add         t1,#WR_ADLDATA          ' pont to value memory
                wrlong      wrvalue,t1              ' save to hub
                mov         t1,par                  ' go get the parameter again
                add         t1,#WR_ADVALID          ' point to the write status
                wrlong      wrstatus,t1             ' now send code 
                andn        outa, LED_P27
sendcmd_ret     ret

' toggle the Init to increment address counter
Init_Toggle     xor         $,$ NR,WC
                muxc        outa,U4_INIT_PIN        ' make sure the Init is low
                muxnc       outa,U4_INIT_PIN        ' now go high 
                call        #please_wait            ' wait a bit
                muxc        outa,U4_INIT_PIN        ' now Init low again
                add         address_count,#1        ' let increment shadow address counter
Init_Toggle_ret
                ret

address_inc     cmp         data_parameter,#0   WZ
        IF_NZ   call        #Init_Toggle
        IF_NZ   djnz        data_parameter,#address_inc
address_inc_ret
                ret

' Toggle sel in pin for memory write
Selin           xor         $,$ NR,WC
                muxc        outa,U4_SELIN_PIN       ' make sure we are low
                muxnc       outa,U4_SELIN_PIN       ' pin high
                call        #please_wait
                muxc        outa,U4_SELIN_PIN       ' pin low
Selin_ret       ret

' Toggle the strobe pin 
Strobe          xor         $,$ NR,WC
                muxc        outa,U3_STROBE_PIN
                muxnc       outa,U3_STROBE_PIN
                'call        #please_wait
                muxc        outa,U3_STROBE_PIN
Strobe_ret      ret

' Toggle the line feed pin
LineFeed        xor         $,$ NR,WC
                muxc        outa,U3_LINEFEED_PIN
                muxnc       outa,U3_LINEFEED_PIN
                'call        #please_wait
                muxc        outa,U3_LINEFEED_PIN
LineFeed_ret    ret


' Very simple dealy routine
please_wait     mov         delay,cnt
                add         delay,HOWLONG
                waitcnt     delay,HOWLONG
please_wait_ret
                ret

                
'
'                        33222222 22221111 11111100 00000000
'                        10987654 32109876 54321098 76543210
VALID_BIT       long    %10000000_00000000_00000000_00000000    ' Validation bit
                                                                ' 
U2_OE_PIN       long    %00000000_00000010_00000000_00000000    ' P17 OE U2
U2_DIR_PIN      long    %00000000_10000000_00000000_00000000    ' P23 DIR U2
U2_DATA_PIN     long    %00000000_00000000_00000000_11111111    ' P7--P0 Data Bus U2

'                        33222222 22221111 11111100 00000000
'                        10987654 32109876 54321098 76543210
U3_DIR_PIN      long    %00000000_00000100_00000000_00000000    ' P18 DIR U3
U3_STROBE_PIN   long    %00000000_00000000_00000001_00000000    ' P8 STROBE U3
U3_LINEFEED_PIN long    %00000000_00000000_00000010_00000000    ' P9 LINEFEED U3

'                        33222222 22221111 11111100 00000000
'                        10987654 32109876 54321098 76543210
U4_DIR_PIN      long    %00000000_00001000_00000000_00000000    ' P19 DIR U4
U4_INIT_PIN     long    %00000000_00000000_00000100_00000000    ' P10 INIT U4
U4_SELIN_PIN    long    %00000000_00000000_00001000_00000000    ' P11 SELIN U4

'                        33222222 22221111 11111100 00000000
'                        10987654 32109876 54321098 76543210
U5_DIR_PIN      long    %00000000_00010000_00000000_00000000    ' P20 DIR U5
U5_BUSY_PIN     long    %00000000_00000000_00100000_00000000    ' P13 BUSY U5
U5_ACK_PIN      long    %00000000_00000000_00010000_00000000    ' P12 ACK U5

'                        33222222 22221111 11111100 00000000
'                        10987654 32109876 54321098 76543210
U6_DIR_PIN      long    %00000000_00100000_00000000_00000000    ' P21 DIR U6
U6_SELECT_PIN   long    %00000000_00000000_10000000_00000000    ' P15 SELECT U6
U6_PEND_PIN     long    %00000000_00000000_01000000_00000000    ' P14 PEND U6

'                        33222222 22221111 11111100 00000000
'                        10987654 32109876 54321098 76543210
U7_DIR_PIN      long    %00000000_01000000_00000000_00000000    ' P22 DIR U7
U7_ERROR_PIN    long    %00000000_00000001_00000000_00000000    ' P16 ERROR U7


'                        33222222 22221111 11111100 00000000
'                        10987654 32109876 54321098 76543210
INIT_OUT_L      long    %00001100_10001110_00001101_00000000    ' Output Pins start low
INIT_OUT_H      long    %00000000_01110000_00000010_11111111    ' Output Pins start high
INIT_DIR_OUT    long    %00001100_11111110_00001111_11111111    ' DIRA Output pins
INIT_DIR_IN     long    %00000000_00000001_11110000_00000000    ' DIRA Input Pins
                                                                
INPUT_DATA      long    %00000000_00000000_11110000_00000000    ' 4 Bit input mask
                                                                ' 
LED_P26         long    %00000100_00000000_00000000_00000000    ' FLIP PCB LED 
LED_P27         long    %00001000_00000000_00000000_00000000    ' FLIP PCB LED
HOWLONG         long    10                                      ' general delay value

t1              res 1   ' temperary 1
t2              res 1   ' temperary 2
p1              res 1   ' internal parameter 1
data_parameter  res 1   ' parameter for data values
rdstatus        res 1   ' read status 
rdvalue         res 1   ' read value
wrstatus        res 1   ' write status or return status
wrvalue         res 1   ' write value or return value
valid           res 1   '
delay           res 1   ' delay
address_count   res 1   ' try to maintain a count of the address counter
u30_shadow      res 1   ' this is the U30 latch shadow memory
u4_shadow       res 1   ' U4 Shadow memory
u5_shadow       res 1   ' U5 Shadow memory
u6_shadow       res 1   ' U6 Shadow memory

                fit