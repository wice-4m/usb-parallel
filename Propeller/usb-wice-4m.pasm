' 
'     _clkmode = xtal1 + pll16x
con
	_clkmode = 1032
'     _xinfreq = 5_000_000
	_xinfreq = 5000000
'     MAX_LEN = 64
	MAX_LEN = 64
'     Version = $00000006
	Version = 6
pub main
  coginit(0, @entry, 0)
dat
	org	0
entry
	mov	arg01, par wz
	locknew	__lockreg
	wrlong	__lockreg, ptr___lockreg_
 if_ne	jmp	#spininit
	mov	pc, $+2
	call	#LMM_CALL_FROM_COG
	long	@@@_Main
cogexit
	cogid	arg01
	cogstop	arg01
spininit
	mov	sp, arg01
	rdlong	objptr, sp
	add	sp, #4
	rdlong	pc, sp
	wrlong	ptr_hubexit_, sp
	add	sp, #4
	rdlong	arg01, sp
	add	sp, #4
	rdlong	arg02, sp
	add	sp, #4
	rdlong	arg03, sp
	add	sp, #4
	rdlong	arg04, sp
	sub	sp, #12
	jmp	#LMM_LOOP
LMM_LOOP
    rdlong LMM_i1, pc
    add    pc, #4
LMM_i1
    nop
    rdlong LMM_i2, pc
    add    pc, #4
LMM_i2
    nop
    rdlong LMM_i3, pc
    add    pc, #4
LMM_i3
    nop
    rdlong LMM_i4, pc
    add    pc, #4
LMM_i4
    nop
    rdlong LMM_i5, pc
    add    pc, #4
LMM_i5
    nop
    rdlong LMM_i6, pc
    add    pc, #4
LMM_i6
    nop
    rdlong LMM_i7, pc
    add    pc, #4
LMM_i7
    nop
    rdlong LMM_i8, pc
    add    pc, #4
LMM_i8
    nop
LMM_jmptop
    jmp    #LMM_LOOP
pc
    long @@@hubentry
lr
    long 0
hubretptr
    long @@@hub_ret_to_cog
LMM_NEW_PC
    long   0
    ' fall through
LMM_CALL
    rdlong LMM_NEW_PC, pc
    add    pc, #4
LMM_CALL_PTR
    wrlong pc, sp
    add    sp, #4
LMM_JUMP_PTR
    mov    pc, LMM_NEW_PC
    jmp    #LMM_LOOP
LMM_JUMP
    rdlong pc, pc
    jmp    #LMM_LOOP
LMM_RET
    sub    sp, #4
    rdlong pc, sp
    jmp    #LMM_LOOP
LMM_CALL_FROM_COG
    wrlong  hubretptr, sp
    add     sp, #4
    jmp  #LMM_LOOP
LMM_CALL_FROM_COG_ret
    ret
    
LMM_CALL_ret
LMM_CALL_PTR_ret
LMM_JUMP_ret
LMM_JUMP_PTR_ret
LMM_RET_ret
LMM_RA
    long	0
    
LMM_FCACHE_LOAD
    rdlong FCOUNT_, pc
    add    pc, #4
    mov    ADDR_, pc
    sub    LMM_ADDR_, pc
    tjz    LMM_ADDR_, #a_fcachegoaddpc
    movd   a_fcacheldlp, #LMM_FCACHE_START
    shr    FCOUNT_, #2
a_fcacheldlp
    rdlong 0-0, pc
    add    pc, #4
    add    a_fcacheldlp,inc_dest1
    djnz   FCOUNT_,#a_fcacheldlp
    '' add in a JMP back out of LMM
    ror    a_fcacheldlp, #9
    movd   a_fcachecopyjmp, a_fcacheldlp
    rol    a_fcacheldlp, #9
a_fcachecopyjmp
    mov    0-0, LMM_jmptop
a_fcachego
    mov    LMM_ADDR_, ADDR_
    jmpret LMM_RETREG,#LMM_FCACHE_START
a_fcachegoaddpc
    add    pc, FCOUNT_
    jmp    #a_fcachego
LMM_FCACHE_LOAD_ret
    ret
inc_dest1
    long (1<<9)
LMM_LEAVE_CODE
    jmp LMM_RETREG
LMM_ADDR_
    long 0
ADDR_
    long 0
FCOUNT_
    long 0
COUNT_
    long 0
prcnt_
    long 0
pushregs_
      movd  :write, #local01
      mov   prcnt_, COUNT_ wz
  if_z jmp  #pushregs_done_
:write
      wrlong 0-0, sp
      add    :write, inc_dest1
      add    sp, #4
      djnz   prcnt_, #:write
pushregs_done_
      wrlong COUNT_, sp
      add    sp, #4
      wrlong fp, sp
      add    sp, #4
      mov    fp, sp
pushregs__ret
      ret
popregs_
      sub   sp, #4
      rdlong fp, sp
      sub   sp, #4
      rdlong COUNT_, sp wz
  if_z jmp  #popregs__ret
      add   COUNT_, #local01
      movd  :read, COUNT_
      sub   COUNT_, #local01
:loop
      sub    :read, inc_dest1
      sub    sp, #4
:read
      rdlong 0-0, sp
      djnz   COUNT_, #:loop
popregs__ret
      ret

unsmultiply_
       mov    itmp2_, #0
       jmp    #do_multiply_

multiply_
       mov    itmp2_, muldiva_
       xor    itmp2_, muldivb_
       abs    muldiva_, muldiva_
       abs    muldivb_, muldivb_
do_multiply_
	mov    result1, #0
mul_lp_
	shr    muldivb_, #1 wc,wz
 if_c	add    result1, muldiva_
	shl    muldiva_, #1
 if_ne	jmp    #mul_lp_
       shr    itmp2_, #31 wz
       negnz  muldiva_, result1
multiply__ret
	ret
' code originally from spin interpreter, modified slightly

unsdivide_
       mov     itmp2_,#0
       jmp     #udiv__

divide_
       abs     muldiva_,muldiva_     wc       'abs(x)
       muxc    itmp2_,divide_haxx_            'store sign of x (mov x,#1 has bits 0 and 31 set)
       abs     muldivb_,muldivb_     wc,wz    'abs(y)
 if_z  jmp     #divbyzero__
 if_c  xor     itmp2_,#1                      'store sign of y
udiv__
divide_haxx_
        mov     itmp1_,#1                    'unsigned divide (bit 0 is discarded)
        mov     DIVCNT,#32
mdiv__
        shr     muldivb_,#1        wc,wz
        rcr     itmp1_,#1
 if_nz   djnz    DIVCNT,#mdiv__
mdiv2__
        cmpsub  muldiva_,itmp1_        wc
        rcl     muldivb_,#1
        shr     itmp1_,#1
        djnz    DIVCNT,#mdiv2__
        shr     itmp2_,#31       wc,wz    'restore sign
        negnz   muldiva_,muldiva_         'remainder
        negc    muldivb_,muldivb_ wz      'division result
divbyzero__
divide__ret
unsdivide__ret
	ret
DIVCNT
	long	0

__lockreg
	long	0
fp
	long	0
imm_1000000000_
	long	1000000000
imm_115200_
	long	115200
imm_2048_
	long	2048
imm_2440_
	long	2440
imm_2456_
	long	2456
imm_2460_
	long	2460
imm_2461_
	long	2461
imm_2462_
	long	2462
imm_2463_
	long	2463
imm_2464_
	long	2464
imm_2792_
	long	2792
imm_4294967295_
	long	-1
imm_512_
	long	512
imm_65532_
	long	65532
itmp1_
	long	0
itmp2_
	long	0
objptr
	long	@@@objmem
ptr_L__0131_
	long	@@@LR__0092
ptr_L__0132_
	long	@@@LR__0093
ptr_L__0133_
	long	@@@LR__0094
ptr__Parallax_serial_terminal_dat__
	long	@@@_Parallax_serial_terminal_dat_
ptr___lockreg_
	long	@@@__lockreg
ptr__parallel_dat__
	long	@@@_parallel_dat_
ptr_hubexit_
	long	@@@hubexit
result1
	long	0
sp
	long	@@@stackspace
COG_BSS_START
	fit	496
hub_ret_to_cog
	jmp	#LMM_CALL_FROM_COG_ret
hubentry

'    
' 
' PUB Main | data
_Main
	mov	COUNT_, #6
	call	#pushregs_
'     term.Start(115200)
	mov	arg01, imm_115200_
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Start
	mov	local01, result1
'     term.Str(String_Version)
	call	#LMM_CALL
	long	@@@_String_Version
	mov	arg01, result1
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Str
'     term.NewLine
' {{Send cursor to new line (carriage return plus line feed).}}
'   
'   Char(NL)
	mov	arg01, #13
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
'     term.Str (string("Wice V0.6"))
	mov	arg01, ptr_L__0131_
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Str
'     term.NewLine
' {{Send cursor to new line (carriage return plus line feed).}}
'   
'   Char(NL)
	mov	arg01, #13
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
'     term.Str (string("Baud = 115200"))
	mov	arg01, ptr_L__0132_
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Str
'     term.NewLine
' {{Send cursor to new line (carriage return plus line feed).}}
'   
'   Char(NL)
	mov	arg01, #13
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
'     wice.Start
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Start
	sub	objptr, imm_2440_
'     term.Str (string("Wice Started"))
	mov	arg01, ptr_L__0133_
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Str
'     term.NewLine
' {{Send cursor to new line (carriage return plus line feed).}}
'   
'   Char(NL)
	mov	arg01, #13
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
' 
'     repeat
LR__0001
'         case term.CharIn
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
	mov	local02, result1 wz
 if_e	add	pc, #4*(LR__0002 - ($+1))
	cmp	local02, #1 wz
 if_e	add	pc, #4*(LR__0003 - ($+1))
	cmp	local02, #2 wz
 if_e	add	pc, #4*(LR__0004 - ($+1))
	cmp	local02, #3 wz
 if_e	add	pc, #4*(LR__0005 - ($+1))
	cmp	local02, #4 wz
 if_e	add	pc, #4*(LR__0006 - ($+1))
	cmp	local02, #5 wz
 if_e	add	pc, #4*(LR__0007 - ($+1))
	cmp	local02, #6 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0008
	cmp	local02, #7 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0009
	cmp	local02, #8 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0010
	cmp	local02, #9 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0011
	cmp	local02, #10 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0012
	cmp	local02, #11 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0013
	cmp	local02, #12 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0014
	cmp	local02, #13 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0015
	cmp	local02, #14 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0016
	cmp	local02, #15 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0017
	cmp	local02, #16 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0018
	cmp	local02, #17 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0019
	cmp	local02, #18 wz
 if_e	sub	pc, #4*(($+1) - LR__0001)
	cmp	local02, #48 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0020
	cmp	local02, #49 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0021
	cmp	local02, #50 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0022
	cmp	local02, #51 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0023
	cmp	local02, #52 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0024
	cmp	local02, #53 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0025
	cmp	local02, #54 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0026
	cmp	local02, #55 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0027
	cmp	local02, #56 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0028
	cmp	local02, #57 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0029
	cmp	local02, #65 wz
 if_e	rdlong	pc,pc
	long	@@@LR__0030
	sub	pc, #4*(($+1) - LR__0001)
LR__0002
'             0 : 'return 0 more of communication test
'                 term.Char (0)
	mov	arg01, #0
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	sub	pc, #4*(($+1) - LR__0001)
LR__0003
' 
'             1 : 'read the status flag
'                 data := wice.Status
'     retval := flags
	add	objptr, imm_2464_
	rdbyte	result1, objptr
	sub	objptr, imm_2464_
	mov	local03, result1
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	sub	pc, #4*(($+1) - LR__0001)
LR__0004
' 
'             2 : 'reset address
'                 data := wice.Reset_Address
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Reset_Address
	sub	objptr, imm_2440_
	mov	local03, result1
'                 term.Char (data.byte[0])
	mov	arg01, local03
	and	arg01, #255
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
'                 term.Char (data.byte[1])
	mov	arg01, local03
	shr	arg01, #8
	and	arg01, #255
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
'                 term.Char (data.byte[2])
	mov	arg01, local03
	shr	arg01, #16
	and	arg01, #255
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
'                 term.Char (data.byte[3])
	mov	local01, local03
	shr	local01, #24
	mov	arg01, local01
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	sub	pc, #4*(($+1) - LR__0001)
LR__0005
' 
'             3 : 'read memory
'                 data := wice.Read_Memory(0)
	mov	arg01, #0
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Read_Memory
	sub	objptr, imm_2440_
	mov	local01, result1
	mov	local03, local01
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0006
' 
'             4 : 'read memory and increment address
'                 data := wice.Read_Memory (1)
	mov	arg01, #1
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Read_Memory
	sub	objptr, imm_2440_
	mov	local01, result1
	mov	local03, local01
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0007
' 
'             5 : ' read shadow address
'                 data := wice.Read_Address
'     result := a_shadow
	add	objptr, imm_2456_
	rdlong	result1, objptr
	sub	objptr, imm_2456_
	mov	local03, result1
'                 SendAddress(data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_SendAddress
	rdlong	pc,pc
	long	@@@LR__0001
LR__0008
' 
'             6 : ' open port for reading and writing
'                 data := term.CharIn
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
	mov	arg01, result1
'                 data &= 1
	and	arg01, #1
'                 wice.Open (data)
	add	objptr, imm_2440_
'     Enable_Strobe(data)
	call	#LMM_CALL
	long	@@@_Wice_4m_Enable_Strobe
'     wice_active_flag(true)
	neg	arg01, #1
	call	#LMM_CALL
	long	@@@_Wice_4m_wice_active_flag
'                 data := wice.Status
'     retval := flags
	add	objptr, #24
	rdbyte	result1, objptr
	sub	objptr, imm_2464_
	mov	local03, result1
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0009
' 
'             7 : ' Close any port
'                 wice.Close
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Close
'                 data := wice.Status
'     retval := flags
	add	objptr, #24
	rdbyte	result1, objptr
	sub	objptr, imm_2464_
	mov	local03, result1
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0010
' 
'             8 : ' write to wice memory
'                 data := term.CharIn
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
	mov	arg01, result1
'                 data := wice.Write_Memory (data , 0)
	mov	local01, #0
	mov	arg02, #0
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Write_Memory
	sub	objptr, imm_2440_
	mov	local04, result1
	mov	local03, local04
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0011
' 
'             9: ' write to wice memory and increment address
'                 data := term.CharIn
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
	mov	arg01, result1
'                 data := wice.Write_Memory (data , 1)
	mov	local01, #1
	mov	arg02, #1
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Write_Memory
	sub	objptr, imm_2440_
	mov	local04, result1
	mov	local03, local04
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0012
' 
'             10: ' 0A Read the U30 Shadow 
'                 data := wice.LF_Read
'     retval := U30_Shadow
	add	objptr, imm_2460_
	rdbyte	result1, objptr
	sub	objptr, imm_2460_
	mov	local03, result1
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0013
' 
'             11: ' 0B Read the U4 shadow
'                 data := wice.Read_U4_Shadow
'     result := U4_Shadow
	add	objptr, imm_2461_
	rdbyte	result1, objptr
	sub	objptr, imm_2461_
	mov	local03, result1
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0014
' 
'             12: ' 0C Read the U5 shadow
'                 data := wice.Read_U5_Shadow
'     result := U5_Shadow
	add	objptr, imm_2462_
	rdbyte	result1, objptr
	sub	objptr, imm_2462_
	mov	local03, result1
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0015
' 
'             13: ' 0D Read the U6 shadow
'                 data := wice.Read_U6_Shadow
'     result := U6_Shadow
	add	objptr, imm_2463_
	rdbyte	result1, objptr
	sub	objptr, imm_2463_
	mov	local03, result1
'                 term.Char (data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0016
' 
'             14: ' 0E Write value to U30 shadow latch
'                 data := term.CharIn
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
	mov	arg01, result1
'                 data := wice.Write_U30_Shadow (data)
	add	objptr, imm_2440_
'     LF_Write(data)
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
'     result := U30_Shadow
	add	objptr, #20
	rdbyte	result1, objptr
	sub	objptr, imm_2460_
	mov	local01, result1
	mov	local03, local01
'                 term.Char(data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0017
' 
'             15: ' 0F Write value to U4 Shadow Latch
'                 data := term.CharIn
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
'                 data := wice.Write_U4_Shadow (data)
	add	objptr, imm_2440_
'     Strobe_Write(0,data)
	mov	arg02, result1
	mov	arg01, #0
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'     result := Read_U4_Shadow
'     result := U4_Shadow
	add	objptr, #21
	rdbyte	result1, objptr
	sub	objptr, imm_2461_
	mov	local01, result1
	mov	local03, local01
'                 term.Char(data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0018
' 
'             16: ' 10 Write value to U5 Shadow Latch
'                 data := term.CharIn
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
'                 data := wice.Write_U5_Shadow (data)
	add	objptr, imm_2440_
'     Strobe_Write(1,data)
	mov	arg02, result1
	mov	arg01, #1
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'     result := Read_U5_Shadow
'     result := U5_Shadow
	add	objptr, #22
	rdbyte	result1, objptr
	sub	objptr, imm_2462_
	mov	local01, result1
	mov	local03, local01
'                 term.Char(data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0019
' 
'             17: ' 11 Write value to U6 Shadow Latch
'                 data := term.CharIn
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
'                 data := wice.Write_U6_Shadow (data)
	add	objptr, imm_2440_
'     Strobe_Write(2,data)
	mov	arg02, result1
	mov	arg01, #2
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'     result := Read_U6_Shadow
'     result := U6_Shadow
	add	objptr, #23
	rdbyte	result1, objptr
	sub	objptr, imm_2463_
	mov	local01, result1
	mov	local03, local01
'                 term.Char(data)
	mov	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	rdlong	pc,pc
	long	@@@LR__0001
LR__0020
' 
'             18: ' 12 get the firmware version
'                 
' 
' 
'             $30 : 'repeat code 0
'                 term.Hex (0, 2)
	mov	local01, #2
	mov	arg01, #0
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
LR__0021
' 
'             $31 : ' read the status flags
'                 data := wice.Status
'     retval := flags
	add	objptr, imm_2464_
	rdbyte	result1, objptr
	sub	objptr, imm_2464_
	mov	local03, result1
'                 term.Hex (data, 2)
	mov	local01, #2
	mov	arg01, local03
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
LR__0022
'             $32 : ' reset address
'                 data := wice.Reset_Address
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Reset_Address
	sub	objptr, imm_2440_
	mov	local03, result1
'                 term.Hex (data, 8)
	mov	local01, #8
	mov	arg01, local03
	mov	arg02, #8
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
LR__0023
' 
'             $33 : 'read memory
'                 data := wice.Read_Memory(0)
	mov	arg01, #0
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Read_Memory
	sub	objptr, imm_2440_
	mov	local03, result1
'                 term.Hex (data, 2)
	mov	local01, #2
	mov	arg01, local03
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
LR__0024
'             $34 : ' read memory increment address
'                 data := wice.Read_Memory (1)
	mov	arg01, #1
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Read_Memory
	sub	objptr, imm_2440_
	mov	local03, result1
'                 term.Hex (data, 2)
	mov	local01, #2
	mov	arg01, local03
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
LR__0025
'             $35 : ' read the shadow address
'                 data := wice.Read_Address
'     result := a_shadow
	add	objptr, imm_2456_
	rdlong	result1, objptr
	sub	objptr, imm_2456_
	mov	local03, result1
'                 term.Hex (data, 8)
	mov	local01, #8
	mov	arg01, local03
	mov	arg02, #8
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
LR__0026
'             $36 : ' open port for reading and writing
'                 data := term.CharIn
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
	mov	arg01, result1
'                 data &= 1
	and	arg01, #1
'                 wice.Open (data)
	add	objptr, imm_2440_
'     Enable_Strobe(data)
	call	#LMM_CALL
	long	@@@_Wice_4m_Enable_Strobe
'     wice_active_flag(true)
	neg	arg01, #1
	call	#LMM_CALL
	long	@@@_Wice_4m_wice_active_flag
'                 data := wice.Status
'     retval := flags
	add	objptr, #24
	rdbyte	result1, objptr
	sub	objptr, imm_2464_
	mov	local03, result1
'                 term.Hex (data, 2)
	mov	local01, #2
	mov	arg01, local03
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
LR__0027
' 
'             $37 : ' close any port
'                 wice.Close
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Close
'                 data := wice.Status
'     retval := flags
	add	objptr, #24
	rdbyte	result1, objptr
	sub	objptr, imm_2464_
	mov	local03, result1
'                 term.Hex (data, 2)
	mov	local01, #2
	mov	arg01, local03
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
LR__0028
' 
'             $38 : ' write to wice memory
'                 term.StrInMax (@pstr, 2)
	mov	arg01, objptr
	add	arg01, imm_2792_
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_StrInMax
'                 data := num.StrToBase(@pstr,16)
	mov	arg01, objptr
	add	arg01, imm_2792_
	mov	arg02, #16
	call	#LMM_CALL
	long	@@@_String_integer_StrToBase
	mov	local05, result1
'                 data := wice.Write_Memory (data , 0)
	mov	arg01, local05
	mov	arg02, #0
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Write_Memory
	sub	objptr, imm_2440_
	mov	local04, result1
	mov	local03, local04
'                 term.Hex (data, 2)
	mov	local01, #2
	mov	arg01, local03
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
LR__0029
' 
'             $39 : ' write to wice memory increment address
'                 term.StrInMax (@pstr,2)
	mov	arg01, objptr
	add	arg01, imm_2792_
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_StrInMax
'                 data := num.StrToBase (@pstr,16)
	mov	arg01, objptr
	add	arg01, imm_2792_
	mov	arg02, #16
	call	#LMM_CALL
	long	@@@_String_integer_StrToBase
	mov	local05, result1
'                 data := wice.Write_Memory (data , 1)
	mov	arg01, local05
	mov	arg02, #1
	add	objptr, imm_2440_
	call	#LMM_CALL
	long	@@@_Wice_4m_Write_Memory
	sub	objptr, imm_2440_
	mov	local04, result1
	mov	local03, local04
'                 term.Hex (data,2)
	mov	local01, #2
	mov	arg01, local03
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
LR__0030
' 
'             $41 : ' read U30 Shadow
'                 data := wice.LF_Read
'     retval := U30_Shadow
	add	objptr, imm_2460_
	rdbyte	result1, objptr
	sub	objptr, imm_2460_
	mov	local06, result1
'                 term.Hex (data,2)                
	mov	local01, #2
	mov	arg01, local06
	mov	arg02, #2
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Hex
	rdlong	pc,pc
	long	@@@LR__0001
	mov	sp, fp
	call	#popregs_
_Main_ret
	call	#LMM_RET

' 
'             'OTHER:
'                 'term.Char (di)
'                
' PRI SendAddress(data) | Index
_SendAddress
	mov	COUNT_, #4
	call	#pushregs_
	add	sp, #8
	wrlong	arg01, fp
' {{ get two bytes from serial to make word }}
'  
'     repeat Index from 3 to 0
	mov	local01, #3
	add	fp, #4
	wrlong	local01, fp
	sub	fp, #4
LR__0031
'         term.Char (data.byte[Index])
	mov	local02, fp
	add	fp, #4
	rdlong	local03, fp
	sub	fp, #4
	add	local03, local02
	rdbyte	arg01, local03
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	add	fp, #4
	rdlong	local04, fp
	sub	local04, #1
	wrlong	local04, fp
	sub	fp, #4
	cmps	local04, #0 wc,wz
 if_ae	sub	pc, #4*(($+1) - LR__0031)
	mov	sp, fp
	call	#popregs_
_SendAddress_ret
	call	#LMM_RET

' 
' 
' PRI String_Version | ver
_String_Version
	mov	COUNT_, #1
	call	#pushregs_
'     ver := Version
' 
'     strg.Combine(@pstr,num.Dec (ver.byte[3]))
	add	objptr, imm_2792_
	mov	local01, objptr
	mov	arg01, #0
	sub	objptr, #324
	call	#LMM_CALL
	long	@@@_String_integer_Dec
	mov	arg02, result1
	mov	arg01, local01
	add	objptr, #68
	call	#LMM_CALL
	long	@@@_STRINGS_Combine
'     return pstr
	add	objptr, #256
	rdbyte	result1, objptr
	sub	objptr, imm_2792_
	mov	sp, fp
	call	#popregs_
_String_Version_ret
	call	#LMM_RET

' 
' PUB Start(baudrate) : okay
_Parallax_serial_terminal_Start
	mov	COUNT_, #1
	call	#pushregs_
' {{Start communication with the Parallax Serial Terminal using the Propeller's programming connection.
'   Returns    : True (non-zero) if cog started, or False (0) if no cog is available.}}
' 
'   okay := StartRxTx(31, 30, 0, baudrate)
	mov	arg04, arg01
	mov	arg01, #31
	mov	arg02, #30
	mov	arg03, #0
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_StartRxTx
	mov	local01, result1
'   waitcnt(clkfreq + cnt)                                'Wait 1 second for PST
	rdlong	arg01, #0
	add	arg01, cnt
	waitcnt	arg01, #0
'   Clear                                                 'Clear display
' {{Clear screen and place cursor at top-left.}}
'   
'   Char(CS)
	mov	arg01, #16
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	mov	result1, local01
	mov	sp, fp
	call	#popregs_
_Parallax_serial_terminal_Start_ret
	call	#LMM_RET

' 
' PUB StartRxTx(rxpin, txpin, mode, baudrate) : okay
_Parallax_serial_terminal_StartRxTx
	mov	COUNT_, #5
	call	#pushregs_
	mov	local01, arg01
	mov	local02, arg02
	mov	local03, arg03
	mov	local04, arg04
' {{Start serial communication with designated pins, mode, and baud.
'   Returns    : True (non-zero) if cog started, or False (0) if no cog is available.}}
' 
'   stop
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Stop
'   longfill(@rx_head, 0, 4)
	add	objptr, #4
	mov	arg01, objptr
	sub	objptr, #4
	mov	arg02, #0
	mov	arg03, #4
	call	#LMM_FCACHE_LOAD
	long	(@@@LR__0033-@@@LR__0032)
LR__0032
	wrlong	arg02, arg01
	add	arg01, #4
	djnz	arg03, #LMM_FCACHE_START + (LR__0032 - LR__0032)
LR__0033
'   longmove(@rx_pin, @rxpin, 3)
	add	objptr, #20
	wrlong	local01, objptr
	add	objptr, #4
	wrlong	local02, objptr
	add	objptr, #4
	wrlong	local03, objptr
	sub	objptr, #28
'   bit_ticks := clkfreq / baudrate
	rdlong	muldiva_, #0
	mov	muldivb_, local04
	call	#divide_
	add	objptr, #32
	wrlong	muldivb_, objptr
'   buffer_ptr := @rx_buffer
	add	objptr, #8
	mov	local05, objptr
	sub	objptr, #4
	wrlong	local05, objptr
'   okay := cog := cognew(@entry, @rx_head) + 1
	mov	arg02, ptr__Parallax_serial_terminal_dat__
	sub	objptr, #32
	mov	arg03, objptr
	sub	objptr, #4
	mov	arg01, #30
	call	#LMM_CALL
	long	@@@__system___coginit
	add	result1, #1
	wrlong	result1, objptr
	mov	sp, fp
	call	#popregs_
_Parallax_serial_terminal_StartRxTx_ret
	call	#LMM_RET

' 
' PUB Stop
_Parallax_serial_terminal_Stop
	mov	COUNT_, #2
	call	#pushregs_
' {{Stop serial communication; frees a cog.}}
' 
'   if cog
	rdlong	local01, objptr wz
 if_e	add	pc, #4*(LR__0034 - ($+1))
'     cogstop(cog~ - 1)
	rdlong	arg01, objptr
	mov	local02, #0
	wrlong	local02, objptr
	sub	arg01, #1
	cogstop	arg01
	mov	result1, #0
LR__0034
'   longfill(@rx_head, 0, 9)
	add	objptr, #4
	mov	arg01, objptr
	sub	objptr, #4
	mov	arg02, #0
	mov	arg03, #9
	call	#LMM_FCACHE_LOAD
	long	(@@@LR__0036-@@@LR__0035)
LR__0035
	wrlong	arg02, arg01
	add	arg01, #4
	djnz	arg03, #LMM_FCACHE_START + (LR__0035 - LR__0035)
LR__0036
	mov	sp, fp
	call	#popregs_
_Parallax_serial_terminal_Stop_ret
	call	#LMM_RET

' 
' PUB Char(bytechr)
_Parallax_serial_terminal_Char
	mov	COUNT_, #3
	call	#pushregs_
	mov	local01, arg01
	call	#LMM_FCACHE_LOAD
	long	(@@@LR__0038-@@@LR__0037)
' {{Send single-byte character.  Waits for room in transmit buffer if necessary.
'     bytechr - character (ASCII byte value) to send.}}
' 
'   repeat until (tx_tail <> ((tx_head + 1) & BUFFER_MASK))
LR__0037
	add	objptr, #12
	rdlong	local02, objptr
	add	local02, #1
	and	local02, #63
	add	objptr, #4
	rdlong	local03, objptr
	sub	objptr, #16
	cmp	local03, local02 wz
 if_e	jmp	#LMM_FCACHE_START + (LR__0037 - LR__0037)
LR__0038
'   tx_buffer[tx_head] := bytechr
	add	objptr, #12
	rdlong	local02, objptr
	add	objptr, #92
	add	local02, objptr
	wrbyte	local01, local02
'   tx_head := (tx_head + 1) & BUFFER_MASK
	sub	objptr, #92
	rdlong	local02, objptr
	add	local02, #1
	and	local02, #63
	wrlong	local02, objptr
' 
'   if rxtx_mode & %1000
	add	objptr, #16
	rdlong	local02, objptr
	sub	objptr, #28
	test	local02, #8 wz
 if_e	add	pc, #4*(LR__0039 - ($+1))
'     CharIn
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
LR__0039
	mov	sp, fp
	call	#popregs_
_Parallax_serial_terminal_Char_ret
	call	#LMM_RET

' 
' PUB CharIn : bytechr
_Parallax_serial_terminal_CharIn
	mov	COUNT_, #1
	call	#pushregs_
' {{Receive single-byte character.  Waits until character received.
'   Returns: $00..$FF}}
' 
'   repeat while (bytechr := RxCheck) < 0
LR__0040
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_RxCheck
	mov	local01, result1
	cmps	local01, #0 wc,wz
 if_b	sub	pc, #4*(($+1) - LR__0040)
	mov	result1, local01
	mov	sp, fp
	call	#popregs_
_Parallax_serial_terminal_CharIn_ret
	call	#LMM_RET

' 
' PUB Str(stringptr)
_Parallax_serial_terminal_Str
	mov	COUNT_, #2
	call	#pushregs_
' {{Send zero terminated string.
'     stringptr - pointer to zero terminated string to send.}}
' 
'   repeat strsize(stringptr)
	mov	local01, arg01
	call	#LMM_CALL
	long	@@@__system____builtin_strlen
	mov	local02, result1 wz
 if_e	add	pc, #4*(LR__0042 - ($+1))
LR__0041
'     Char(byte[stringptr++])
	rdbyte	arg01, local01
	add	local01, #1
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	djnz	local02, #LMM_JUMP
	long	@@@LR__0041
LR__0042
	mov	sp, fp
	call	#popregs_
_Parallax_serial_terminal_Str_ret
	call	#LMM_RET

' 
' PUB StrInMax(stringptr, maxcount)
_Parallax_serial_terminal_StrInMax
	mov	COUNT_, #6
	call	#pushregs_
	mov	local01, arg01
	mov	local02, arg02
' {{Receive a string of characters (either carriage return terminated or maxcount in length) and stores it (zero terminated)
'     maxcount  - maximum length of string to receive, or -1 for unlimited.}}
'     
'   repeat while (maxcount--)                                                     'While maxcount not reached
LR__0043
	mov	local03, local02 wz
	sub	local02, #1
 if_e	add	pc, #4*(LR__0044 - ($+1))
'     if (byte[stringptr++] := CharIn) == NL                                      'Get chars until NL
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_CharIn
	wrbyte	result1, local01
	rdbyte	local04, local01
	cmp	local04, #13 wz
'       quit
	add	local01, #1
 if_ne	sub	pc, #4*(($+1) - LR__0043)
LR__0044
'   byte[stringptr+(byte[stringptr-1] == NL)]~                                    'Zero terminate string; overwrite NL or append 0 char
	mov	local03, local01
	mov	local05, #0
	sub	local01, #1
	rdbyte	local04, local01
	cmp	local04, #13 wz
 if_e	neg	local05, #1
	add	local03, local05
	mov	local06, #0
	wrbyte	local06, local03
	mov	sp, fp
	call	#popregs_
_Parallax_serial_terminal_StrInMax_ret
	call	#LMM_RET

'    
' PUB Hex(value, digits)
_Parallax_serial_terminal_Hex
	mov	COUNT_, #3
	call	#pushregs_
	mov	local01, arg02 wz
' {{Send value as hexadecimal characters up to digits in length.
'     digits - number of hexadecimal digits to send.  Will be zero padded if necessary.}}
' 
'   value <<= (8 - digits) << 2
	mov	local02, arg01
	mov	local03, #8
	sub	local03, local01
	shl	local03, #2
	shl	local02, local03
'   repeat digits
 if_e	add	pc, #4*(LR__0046 - ($+1))
LR__0045
'     Char(lookupz((value <-= 4) & $F : "0".."9", "A".."F"))
	rol	local02, #4
	mov	arg01, local02
	and	arg01, #15
	add	ptr__Parallax_serial_terminal_dat__, #336
	mov	arg03, ptr__Parallax_serial_terminal_dat__
	sub	ptr__Parallax_serial_terminal_dat__, #336
	mov	arg02, #0
	mov	arg04, #16
	call	#LMM_CALL
	long	@@@__system___lookup
	mov	arg01, result1
	call	#LMM_CALL
	long	@@@_Parallax_serial_terminal_Char
	djnz	local01, #LMM_JUMP
	long	@@@LR__0045
LR__0046
	mov	sp, fp
	call	#popregs_
_Parallax_serial_terminal_Hex_ret
	call	#LMM_RET

'     
' PRI RxCheck : bytechr
_Parallax_serial_terminal_RxCheck
' {Check if character received; return immediately.
'   Returns: -1 if no byte received, $00..$FF if character received.}
' 
'   bytechr~~
	neg	_var01, #1
'   if rx_tail <> rx_head
	add	objptr, #8
	rdlong	_var02, objptr
	sub	objptr, #4
	rdlong	_var03, objptr
	sub	objptr, #4
	cmp	_var02, _var03 wz
 if_e	add	pc, #4*(LR__0047 - ($+1))
'     bytechr := rx_buffer[rx_tail]
	add	objptr, #8
	rdlong	_var02, objptr
	add	objptr, #32
	add	_var02, objptr
	rdbyte	_var01, _var02
'     rx_tail := (rx_tail + 1) & BUFFER_MASK
	sub	objptr, #32
	rdlong	_var02, objptr
	add	_var02, #1
	and	_var02, #63
	wrlong	_var02, objptr
	sub	objptr, #8
LR__0047
	mov	result1, _var01
_Parallax_serial_terminal_RxCheck_ret
	call	#LMM_RET

' 
' 
' PRI Enable_Strobe(mode)
_Wice_4m_Enable_Strobe
	mov	COUNT_, #3
	call	#pushregs_
	mov	local01, arg01
'     CASE mode
	mov	local02, local01 wz
 if_e	add	pc, #4*(LR__0048 - ($+1))
	cmp	local02, #1 wz
 if_e	add	pc, #4*(LR__0049 - ($+1))
	add	pc, #4*(LR__0050 - ($+1))
LR__0048
'         0:
'             LF_Write($CB)
	mov	arg01, #203
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
'             LF_Write($CF)
	mov	arg01, #207
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
	add	pc, #4*(LR__0050 - ($+1))
LR__0049
'         1:
'             LF_Write($DB)
	mov	arg01, #219
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
'             LF_Write($DF)
	mov	local03, #223
	mov	arg01, #223
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
LR__0050
	mov	sp, fp
	call	#popregs_
_Wice_4m_Enable_Strobe_ret
	call	#LMM_RET

' 
' PRI Inc_Address
_Wice_4m_Inc_Address
	mov	COUNT_, #1
	call	#pushregs_
'         parallel.Init_Line  { increment Address }
'     Init_Off
	call	#LMM_CALL
	long	@@@_parallel_Init_Off
'     Init_On
	call	#LMM_CALL
	long	@@@_parallel_Init_On
'     Init_Off
	call	#LMM_CALL
	long	@@@_parallel_Init_Off
'         a_shadow++ 
	add	objptr, #16
	rdlong	local01, objptr
	add	local01, #1
	wrlong	local01, objptr
	sub	objptr, #16
	mov	sp, fp
	call	#popregs_
_Wice_4m_Inc_Address_ret
	call	#LMM_RET

' 
' PUB LF_Write(data)
_Wice_4m_LF_Write
'     U30_Shadow := data
	add	objptr, #20
	wrbyte	arg01, objptr
	sub	objptr, #20
'     parallel.Data (data)
	call	#LMM_CALL
	long	@@@_parallel_Data
'     parallel.Line_Feed
'     LF_Off
'     OUTA[U3_LINEFEED]~
	andn	outa, imm_512_
'     LF_On
'     OUTA[U3_LINEFEED]~~
	or	outa, imm_512_
'     LF_Off
'     OUTA[U3_LINEFEED]~
	andn	outa, imm_512_
_Wice_4m_LF_Write_ret
	call	#LMM_RET

' 
' 
' PUB Strobe_Write(address,data)
_Wice_4m_Strobe_Write
	mov	COUNT_, #4
	call	#pushregs_
	mov	local01, arg01
	mov	local02, arg02
'     CASE address
	mov	local03, local01 wz
 if_e	add	pc, #4*(LR__0051 - ($+1))
	cmp	local03, #1 wz
 if_e	add	pc, #4*(LR__0052 - ($+1))
	cmp	local03, #2 wz
 if_e	add	pc, #4*(LR__0053 - ($+1))
	cmp	local03, #3 wz
 if_e	add	pc, #4*(LR__0054 - ($+1))
	cmp	local03, #128 wz
 if_e	add	pc, #4*(LR__0055 - ($+1))
	cmp	local03, #129 wz
 if_e	add	pc, #4*(LR__0056 - ($+1))
	cmp	local03, #130 wz
 if_e	add	pc, #4*(LR__0057 - ($+1))
	add	pc, #4*(LR__0058 - ($+1))
LR__0051
'         0:
'             LF_Write(U30_SEL_U4)
	mov	arg01, #68
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
'             U4_Shadow := data
	add	objptr, #21
	wrbyte	local02, objptr
	sub	objptr, #21
	add	pc, #4*(LR__0058 - ($+1))
LR__0052
'         1:
'             LF_Write(U30_SEL_U5)
	mov	arg01, #69
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
'             U5_Shadow := data
	add	objptr, #22
	wrbyte	local02, objptr
	sub	objptr, #22
	add	pc, #4*(LR__0058 - ($+1))
LR__0053
'         2:
'             LF_Write(U30_SEL_U6)
	mov	arg01, #70
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
'             U6_Shadow := data
	add	objptr, #23
	wrbyte	local02, objptr
	sub	objptr, #23
	add	pc, #4*(LR__0058 - ($+1))
LR__0054
'         3:
'             U30_Shadow &= !3
	add	objptr, #20
	rdbyte	local04, objptr
	andn	local04, #3
	wrbyte	local04, objptr
'             U30_Shadow |= U30_DISABLE
	rdbyte	local04, objptr
	or	local04, #3
	wrbyte	local04, objptr
'             LF_Write(U30_Shadow)
	rdbyte	arg01, objptr
	sub	objptr, #20
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
	add	pc, #4*(LR__0058 - ($+1))
LR__0055
' 
'         128:
'             U4_Shadow := data
	add	objptr, #21
	wrbyte	local02, objptr
	sub	objptr, #21
	add	pc, #4*(LR__0058 - ($+1))
LR__0056
'         129:
'             U5_Shadow := data
	add	objptr, #22
	wrbyte	local02, objptr
	sub	objptr, #22
	add	pc, #4*(LR__0058 - ($+1))
LR__0057
'         130:
'             U6_Shadow := data
	add	objptr, #23
	wrbyte	local02, objptr
	sub	objptr, #23
LR__0058
'     parallel.Data (data)
	mov	arg01, local02
	call	#LMM_CALL
	long	@@@_parallel_Data
'     parallel.Strobes
'     Strobe_Off
'     OUTA[U3_STROBE]~
	andn	outa, #256
'     Strobe_On
'     OUTA[U3_STROBE]~~
	or	outa, #256
'     Strobe_Off
'     OUTA[U3_STROBE]~
	andn	outa, #256
	mov	sp, fp
	call	#popregs_
_Wice_4m_Strobe_Write_ret
	call	#LMM_RET

' 
' PUB Write_Memory(data , inc)
_Wice_4m_Write_Memory
	mov	COUNT_, #3
	call	#pushregs_
	mov	local01, arg01
	mov	local02, arg02
	mov	local03, #0
'     if(active)
	call	#LMM_CALL
	long	@@@_Wice_4m_active
	cmp	result1, #0 wz
 if_e	add	pc, #4*(LR__0059 - ($+1))
'         parallel.Data (data)
	mov	arg01, local01
	call	#LMM_CALL
	long	@@@_parallel_Data
'         parallel.Selptr_Line
'     Selptr_Off
'     OUTA[U4_SELIN]~
	andn	outa, imm_2048_
'     Selptr_On
'     OUTA[U4_SELIN]~~
	or	outa, imm_2048_
'     Selptr_Off
'     OUTA[U4_SELIN]~
	andn	outa, imm_2048_
'         result := Read_Memory(inc)
	mov	arg01, local02
	call	#LMM_CALL
	long	@@@_Wice_4m_Read_Memory
	mov	local03, result1
LR__0059
	mov	result1, local03
	mov	sp, fp
	call	#popregs_
_Wice_4m_Write_Memory_ret
	call	#LMM_RET

'    
' 
' PUB Reset_Address | shadow
_Wice_4m_Reset_Address
	mov	COUNT_, #2
	call	#pushregs_
'     if(active)
	call	#LMM_CALL
	long	@@@_Wice_4m_active
	mov	local01, result1 wz
 if_e	add	pc, #4*(LR__0060 - ($+1))
'         shadow := U6_Shadow
	add	objptr, #23
	rdbyte	local02, objptr
	sub	objptr, #23
'         shadow |= |<4
	or	local02, #16
'         Strobe_Write(2,shadow)
	mov	arg01, #2
	mov	arg02, local02
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'         shadow &= !|<4
	andn	local02, #16
'         Strobe_Write(130,shadow)
	mov	arg02, local02
	mov	arg01, #130
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'         a_shadow := 0
	mov	local01, #0
	add	objptr, #16
	wrlong	local01, objptr
'         return a_shadow
	mov	result1, #0
	sub	objptr, #16
	add	pc, #4*(LR__0061 - ($+1))
LR__0060
	mov	result1, #0
LR__0061
	mov	sp, fp
	call	#popregs_
_Wice_4m_Reset_Address_ret
	call	#LMM_RET

' 
' PRI Read_Status : retval
_Wice_4m_Read_Status
	mov	COUNT_, #1
	call	#pushregs_
'     retval |= parallel.Busy_Status
	mov	local01, #0
'     retval := INA[U5_BUSY]
	mov	result1, ina
	shr	result1, #13
	and	result1, #1
	or	local01, result1
'     retval <<= 1
	shl	local01, #1
'     retval |= parallel.Ack_Status
'     retval := INA[U5_ACK]
	mov	result1, ina
	shr	result1, #12
	and	result1, #1
	or	local01, result1
'     retval <<= 1
	shl	local01, #1
'     retval |= parallel.Pend_Status
'     retval := INA[U6_PEND]
	mov	result1, ina
	shr	result1, #14
	and	result1, #1
	or	local01, result1
'     retval <<= 1
	shl	local01, #1
'     retval |= parallel.Select_Status
'     retval := INA[U6_SELECT]
	mov	result1, ina
	shr	result1, #15
	and	result1, #1
	or	result1, local01
	mov	sp, fp
	call	#popregs_
_Wice_4m_Read_Status_ret
	call	#LMM_RET

' 
' PRI active
_Wice_4m_active
'     result := (flags & FLAG_ACTIVE) == FLAG_ACTIVE
	mov	_var01, #0
	add	objptr, #24
	rdbyte	_var02, objptr
	sub	objptr, #24
	test	_var02, #0 wz
 if_e	neg	_var01, #1
	mov	result1, _var01
_Wice_4m_active_ret
	call	#LMM_RET

' 
' PRI wice_active_flag(flag)
_Wice_4m_wice_active_flag
'     if(flag)
	cmp	arg01, #0 wz
 if_e	add	pc, #4*(LR__0062 - ($+1))
'         flags |= |<FLAG_ACTIVE
	add	objptr, #24
	rdbyte	_var01, objptr
	or	_var01, #1
	wrbyte	_var01, objptr
	sub	objptr, #24
'     else
	add	pc, #4*(LR__0063 - ($+1))
LR__0062
'         flags &= !|<FLAG_ACTIVE
	add	objptr, #24
	rdbyte	_var01, objptr
	andn	_var01, #1
	wrbyte	_var01, objptr
	sub	objptr, #24
LR__0063
_Wice_4m_wice_active_flag_ret
	call	#LMM_RET

' 
' PUB Read_Memory(inc):retval
_Wice_4m_Read_Memory
	mov	COUNT_, #3
	call	#pushregs_
	mov	local01, arg01
'     retval := 0
	mov	local02, #0
'     if(active)
	call	#LMM_CALL
	long	@@@_Wice_4m_active
	cmp	result1, #0 wz
 if_e	add	pc, #4*(LR__0067 - ($+1))
'         Strobe_Write(1,$46)
	mov	arg01, #1
	mov	arg02, #70
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'         retval |= Read_Status
	call	#LMM_CALL
	long	@@@_Wice_4m_Read_Status
	or	local02, result1
'         retval <<= 4
	shl	local02, #4
'         Strobe_Write(129,$86)
	mov	arg01, #129
	mov	arg02, #134
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'         retval |= Read_Status
	call	#LMM_CALL
	long	@@@_Wice_4m_Read_Status
	or	local02, result1
'         Strobe_Write(129,$C7)
	mov	arg01, #129
	mov	arg02, #199
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'         if(inc)
	cmp	local01, #0 wz
 if_e	add	pc, #4*(LR__0066 - ($+1))
'             repeat inc
	mov	local03, local01 wz
 if_e	add	pc, #4*(LR__0065 - ($+1))
LR__0064
'                 Inc_Address
	call	#LMM_CALL
	long	@@@_Wice_4m_Inc_Address
	djnz	local03, #LMM_JUMP
	long	@@@LR__0064
LR__0065
LR__0066
LR__0067
	mov	result1, local02
	mov	sp, fp
	call	#popregs_
_Wice_4m_Read_Memory_ret
	call	#LMM_RET

' 
' PUB Close
_Wice_4m_Close
'     LF_Write($4B)
	mov	arg01, #75
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
'     LF_Write($4F)
	mov	arg01, #79
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
'     wice_active_flag(false)
	mov	arg01, #0
	call	#LMM_CALL
	long	@@@_Wice_4m_wice_active_flag
_Wice_4m_Close_ret
	call	#LMM_RET

' 
'         
' PRI LF_Disable
_Wice_4m_LF_Disable
	mov	COUNT_, #1
	call	#pushregs_
'     U30_Shadow := |<U30_PALCLK | |<U30_MODELATCH | U30_DISABLE
	mov	local01, #71
	add	objptr, #20
	wrbyte	local01, objptr
'     LF_Write(U30_Shadow)
	rdbyte	arg01, objptr
	sub	objptr, #20
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Write
	mov	sp, fp
	call	#popregs_
_Wice_4m_LF_Disable_ret
	call	#LMM_RET

' 
' PUB Start | rdata
_Wice_4m_Start
'     result := false
'     parallel.Start
	call	#LMM_CALL
	long	@@@_parallel_Start
' 
'     LF_Disable
	call	#LMM_CALL
	long	@@@_Wice_4m_LF_Disable
'     Open(0)
	mov	arg01, #0
'     Enable_Strobe(data)
	call	#LMM_CALL
	long	@@@_Wice_4m_Enable_Strobe
'     wice_active_flag(true)
	neg	arg01, #1
	call	#LMM_CALL
	long	@@@_Wice_4m_wice_active_flag
' 
'     Strobe_Write(0,$FF) { Initilise U4 Latch and shadow }
	mov	arg01, #0
	mov	arg02, #255
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'     Strobe_Write(1,$C7) { Initilise U5 Latch and Shadow }
	mov	arg01, #1
	mov	arg02, #199
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'     Strobe_Write(2,$11) { initilise U6 Shadow mem }
	mov	arg01, #2
	mov	arg02, #17
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
'     Strobe_Write(130,1) { now set U6 to Default }
	mov	arg01, #130
	mov	arg02, #1
	call	#LMM_CALL
	long	@@@_Wice_4m_Strobe_Write
' 
' 
' '    Read_Memory
' '    parallel.Init_Line  { increment Address }
' '    Reset_Address
' 
' '    if(Test_Memory)
' '        result := true
' 
'     Reset_Address
	call	#LMM_CALL
	long	@@@_Wice_4m_Reset_Address
	mov	result1, #0
_Wice_4m_Start_ret
	call	#LMM_RET

' 
' PRI Init_On
_parallel_Init_On
'     OUTA[U4_INIT]~~
	mov	_var01, #1
	add	ptr__parallel_dat__, #268
	rdlong	_var02, ptr__parallel_dat__
	sub	ptr__parallel_dat__, #268
	shl	_var01, _var02
	or	outa, _var01
_parallel_Init_On_ret
	call	#LMM_RET

' 
' PRI Init_Off
_parallel_Init_Off
'     OUTA[U4_INIT]~
	mov	_var01, #1
	add	ptr__parallel_dat__, #268
	rdlong	_var02, ptr__parallel_dat__
	sub	ptr__parallel_dat__, #268
	shl	_var01, _var02
	andn	outa, _var01
_parallel_Init_Off_ret
	call	#LMM_RET

' 
' PUB Data(value)
_parallel_Data
'     OUTA[7..0] := value
	and	arg01, #255
	mov	_var01, outa
	andn	_var01, #255
	or	_var01, arg01
	mov	outa, _var01
_parallel_Data_ret
	call	#LMM_RET

' 
' 
' PUB Start:okay
_parallel_Start
	mov	COUNT_, #1
	call	#pushregs_
' '   OE_Disable              'Disable OE so chip output floating
' '   DIRA[U2_OE] := 1        'Now disable
' '   Data($FF)               'make all pins high
' '   U2_Dir_Out              'Init direction pin allways make out
' '   DIRA[U2_DIR] := 1       'U2 DIR Pin is now output
' '   DIRA[7..0]~~            'make data pins as out
' '   Data($FF)
' '   OE_Enable
' 
' '   U3_Dir_Out
' 
' '   DIRA[U3_DIR] := 1
' '   Strobe_Off
' '   LF_Off
' '   DIRA[U3_STROBE] := 1   ' Strobe is output
' '   DIRA[U3_LINEFEED] := 1 ' Line feed is output
' 
' '   DIRA[U4_DIR] := 1     ' Init is output
' '   U4_Dir_Out
' '   DIRA[U4_INIT] := 1
' '   Init_Off
' '   DIRA[U4_SELIN] := 1   'SELPTR output
' '   Selptr_Off
' 
' '   DIRA[U5_DIR] := 1
' '   U5_Dir_In
' '   DIRA[U5_ACK] := 0      'ACK is input
' '   DIRA[U5_BUSY] := 0     'Busy is input'
' 
' '   DIRA[U6_DIR] := 1
' '   U6_Dir_In
' '   DIRA[U6_PEND] := 0      'PEND or POUT input
' '   DIRA[U6_SELECT] := 0    'Select is input
' 
' '   DIRA[U7_DIR] := 1
' '   U7_Dir_In
' '   DIRA[U7_ERROR] := 0    ' Error pin input
' 
' '    okay := 1
' 
'     rd_valid := 0               ' make sure all valid bit are cleared
	mov	local01, #0
	wrlong	local01, objptr
'     wr_valid := 0               ' clear all bits to indicate data is not valid
	add	objptr, #4
	wrlong	local01, objptr
	sub	objptr, #4
'  
'     COGNEW(@entry,@rd_valid)
	mov	arg02, ptr__parallel_dat__
	mov	arg03, objptr
	mov	arg01, #30
	call	#LMM_CALL
	long	@@@__system___coginit
'     return okay
	mov	result1, #0
	mov	sp, fp
	call	#popregs_
_parallel_Start_ret
	call	#LMM_RET

' 
' PUB Dec(value)
_String_integer_Dec
	mov	COUNT_, #1
	call	#pushregs_
	mov	local01, arg01
' {{
' }}
' 
'     ClearStr(@nstr, MAX_LEN)                                ' clear output string
	add	objptr, #4
	mov	arg01, objptr
	sub	objptr, #4
	mov	arg02, #64
	call	#LMM_CALL
	long	@@@_String_integer_ClearStr
'     return DecToStr(value)                                  ' return pointer to numeric string
	mov	arg01, local01
	call	#LMM_CALL
	long	@@@_String_integer_DecToStr
	mov	sp, fp
	call	#popregs_
_String_integer_Dec_ret
	call	#LMM_RET

' 
' PRI ClearStr(strAddr, size)
_String_integer_ClearStr
	mov	COUNT_, #1
	call	#pushregs_
' {{
' }}
' 
'     bytefill(strAddr, 0, size)                            ' clear string to zeros
	mov	arg03, arg02 wz
	mov	arg02, #0
 if_e	add	pc, #4*(LR__0070 - ($+1))
	call	#LMM_FCACHE_LOAD
	long	(@@@LR__0069-@@@LR__0068)
LR__0068
	wrbyte	arg02, arg01
	add	arg01, #1
	djnz	arg03, #LMM_FCACHE_START + (LR__0068 - LR__0068)
LR__0069
LR__0070
'     idx~                                                  ' reset index
	mov	local01, #0
	wrlong	local01, objptr
	mov	sp, fp
	call	#popregs_
_String_integer_ClearStr_ret
	call	#LMM_RET

' 
' PRI DecToStr(value) | div, z_pad
_String_integer_DecToStr
' {{
' }}
' 
'     if (value < 0)                                        ' negative value?
	cmps	arg01, #0 wc,wz
 if_ae	add	pc, #4*(LR__0071 - ($+1))
'         -value                                              '   yes, make positive
	neg	arg01, arg01
'         nstr[idx++] := "-"                                  '   and print sign indicator
	rdlong	_var01, objptr
	mov	_var02, _var01
	add	_var02, #1
	wrlong	_var02, objptr
	add	objptr, #4
	add	_var01, objptr
	mov	_var03, #45
	wrbyte	_var03, _var01
	sub	objptr, #4
LR__0071
' 
'     div := 1_000_000_000                                  ' initialize divisor
	mov	_var04, imm_1000000000_
'     z_pad~                                                ' clear zero-pad flag
	mov	_var05, #0
' 
'     repeat 10
	mov	_var06, #10
	call	#LMM_FCACHE_LOAD
	long	(@@@LR__0077-@@@LR__0072)
LR__0072
'         if (value => div)                                   ' printable character?
	cmps	arg01, _var04 wc,wz
 if_b	jmp	#LMM_FCACHE_START + (LR__0073 - LR__0072)
'             nstr[idx++] := (value / div + "0")                '   yes, print ASCII digit
	mov	muldiva_, arg01
	mov	muldivb_, _var04
	call	#divide_
	add	muldivb_, #48
	rdlong	_var07, objptr
	mov	_var01, _var07
	add	_var01, #1
	wrlong	_var01, objptr
	add	objptr, #4
	add	_var07, objptr
	wrbyte	muldivb_, _var07
'             value //= div                                     '   update value
	mov	arg01, muldiva_
'             z_pad~~                                           '   set zflag
	neg	_var05, #1
	sub	objptr, #4
	jmp	#LMM_FCACHE_START + (LR__0076 - LR__0072)
LR__0073
'         elseif z_pad or (div == 1)                          ' printing or last column?
	cmp	_var05, #0 wz
 if_ne	jmp	#LMM_FCACHE_START + (LR__0074 - LR__0072)
	cmp	_var04, #1 wz
 if_ne	jmp	#LMM_FCACHE_START + (LR__0075 - LR__0072)
LR__0074
'             nstr[idx++] := "0"
	rdlong	_var01, objptr
	mov	_var02, _var01
	add	_var02, #1
	wrlong	_var02, objptr
	add	objptr, #4
	add	_var01, objptr
	mov	_var03, #48
	wrbyte	_var03, _var01
	sub	objptr, #4
LR__0075
LR__0076
'         div /= 10
	mov	muldiva_, _var04
	mov	muldivb_, #10
	call	#divide_
	mov	_var04, muldivb_
	sub	_var06, #1
	mov	_var06, _var06 wz
 if_ne	jmp	#LMM_FCACHE_START + (LR__0072 - LR__0072)
LR__0077
' 
'     return @nstr
	add	objptr, #4
	mov	result1, objptr
	sub	objptr, #4
_String_integer_DecToStr_ret
	call	#LMM_RET

' 
' PUB StrToBase(stringptr, base) : value | chr, index
_String_integer_StrToBase
	mov	_var01, arg01
	mov	_var02, arg02
' {{
' }}
' 
'     value := index := 0
	mov	_var03, #0
	mov	_var04, #0
'     repeat until ((chr := byte[stringptr][index++]) == 0)
LR__0078
	mov	_var05, _var03
	add	_var05, _var01
	rdbyte	_var06, _var05 wz
	add	_var03, #1
 if_e	add	pc, #4*(LR__0079 - ($+1))
'         chr := -15 + --chr & %11011111 + 39*(chr > 56)                      ' Make "0"-"9","A"-"F","a"-"f" be 0 - 15, others out of range
	sub	_var06, #1
	mov	_var07, _var06
	and	_var07, #223
	neg	_var08, #15
	add	_var08, _var07
	mov	_var09, #0
	cmps	_var06, #56 wc,wz
 if_a	neg	_var09, #1
	mov	muldiva_, _var09
	mov	muldivb_, #39
	call	#multiply_
	add	_var08, muldiva_
	mov	_var06, _var08
'         if (chr > -1) and (chr < base)                                      ' Accumulate valid values into result; ignore others
	cmps	_var06, imm_4294967295_ wc,wz
 if_be	sub	pc, #4*(($+1) - LR__0078)
	cmps	_var06, _var02 wc,wz
 if_ae	sub	pc, #4*(($+1) - LR__0078)
'             value := value * base + chr
	mov	muldiva_, _var04
	mov	muldivb_, _var02
	call	#multiply_
	mov	_var04, muldiva_
	add	_var04, _var06
	sub	pc, #4*(($+1) - LR__0078)
LR__0079
'     if (base == 10) and (byte[stringptr] == "-")                            ' If decimal, address negative sign; ignore otherwise
	cmp	_var02, #10 wz
 if_ne	add	pc, #4*(LR__0080 - ($+1))
	rdbyte	_var08, _var01
	cmp	_var08, #45 wz
'         value := - value
 if_e	mov	_var08, _var04
 if_e	neg	_var08, _var08
 if_e	mov	_var04, _var08
LR__0080
	mov	result1, _var04
_String_integer_StrToBase_ret
	call	#LMM_RET

' 
' PUB Combine (str1Addr, str2Addr) | size1, size2, tmp
_STRINGS_Combine
	mov	COUNT_, #6
	call	#pushregs_
	mov	local01, arg01
	mov	local02, arg02
' {{Appends str2 to the end of str1
' }}
'   size1 := strsize(str1Addr)
	mov	arg01, local01
	call	#LMM_CALL
	long	@@@__system____builtin_strlen
	mov	local03, result1
'   size2 := strsize(str2Addr)                                 
	mov	arg01, local02
	call	#LMM_CALL
	long	@@@__system____builtin_strlen
	mov	local04, result1
' 
'   bytemove(@ostr, str1Addr, size1 <# constant(STR_MAX_LENGTH - 1))              ' move string 1
	mov	arg01, objptr
	mov	arg03, local03
	maxs	arg03, #127
	mov	arg02, local01
	call	#LMM_CALL
	long	@@@__system__bytemove
'   IF (size1 < STR_MAX_LENGTH)
	cmps	local03, #128 wc,wz
 if_ae	add	pc, #4*(LR__0081 - ($+1))
'     bytemove(@ostr + (size1 <# STR_MAX_LENGTH), str2Addr, (constant(STR_MAX_LENGTH - 1) - size1) <# size2) ' move string 2
	mov	arg01, objptr
	mov	local05, local03
	maxs	local05, #128
	add	arg01, local05
	mov	arg03, #127
	sub	arg03, local03
	maxs	arg03, local04
	mov	arg02, local02
	call	#LMM_CALL
	long	@@@__system__bytemove
LR__0081
' 
'   ostr[(size1 + size2) <# STR_MAX_LENGTH] := 0                                  ' terminate string
	add	local03, local04
	maxs	local03, #128
	add	local03, objptr
	mov	local06, #0
	wrbyte	local06, local03
'   RETURN @ostr                                                                                          
	mov	result1, objptr
	mov	sp, fp
	call	#popregs_
_STRINGS_Combine_ret
	call	#LMM_RET
hubexit
	jmp	#cogexit

__system___coginit
	and	arg03, imm_65532_
	shl	arg03, #16
	and	arg02, imm_65532_
	shl	arg02, #2
	or	arg03, arg02
	and	arg01, #15
	or	arg03, arg01
	coginit	arg03 wc,wr
 if_b	neg	arg03, #1
	mov	result1, arg03
__system___coginit_ret
	call	#LMM_RET

__system__bytemove
	mov	_var01, arg01
	cmps	arg01, arg02 wc,wz
 if_ae	add	pc, #4*(LR__0084 - ($+1))
	mov	_var02, arg03 wz
 if_e	add	pc, #4*(LR__0088 - ($+1))
	call	#LMM_FCACHE_LOAD
	long	(@@@LR__0083-@@@LR__0082)
LR__0082
	rdbyte	_var03, arg02
	wrbyte	_var03, arg01
	add	arg01, #1
	add	arg02, #1
	djnz	_var02, #LMM_FCACHE_START + (LR__0082 - LR__0082)
LR__0083
	add	pc, #4*(LR__0088 - ($+1))
LR__0084
	add	arg01, arg03
	add	arg02, arg03
	mov	_var04, arg03 wz
 if_e	add	pc, #4*(LR__0087 - ($+1))
	call	#LMM_FCACHE_LOAD
	long	(@@@LR__0086-@@@LR__0085)
LR__0085
	sub	arg01, #1
	sub	arg02, #1
	rdbyte	_var03, arg02
	wrbyte	_var03, arg01
	djnz	_var04, #LMM_FCACHE_START + (LR__0085 - LR__0085)
LR__0086
LR__0087
LR__0088
	mov	result1, _var01
__system__bytemove_ret
	call	#LMM_RET

__system____builtin_strlen
	mov	_var01, #0
	call	#LMM_FCACHE_LOAD
	long	(@@@LR__0090-@@@LR__0089)
LR__0089
	rdbyte	_var02, arg01 wz
 if_ne	add	_var01, #1
 if_ne	add	arg01, #1
 if_ne	jmp	#LMM_FCACHE_START + (LR__0089 - LR__0089)
LR__0090
	mov	result1, _var01
__system____builtin_strlen_ret
	call	#LMM_RET

__system___lookup
	mov	_var01, arg01
	mov	_var02, arg02
	mov	_var03, arg03
	mov	_var04, arg04
	sub	_var01, _var02
	cmps	_var01, #0 wc,wz
 if_b	add	pc, #4*(LR__0091 - ($+1))
	cmps	_var01, _var04 wc,wz
 if_ae	add	pc, #4*(LR__0091 - ($+1))
	mov	_var05, _var01
	shl	_var05, #2
	add	_var05, _var03
	rdlong	result1, _var05
	rdlong	pc,pc
	long	@@@__system___lookup_ret
LR__0091
	mov	result1, #0
__system___lookup_ret
	call	#LMM_RET

LR__0092
	byte	"Wice V0.6"
	byte	0
LR__0093
	byte	"Baud = 115200"
	byte	0
LR__0094
	byte	"Wice Started"
	byte	0
	long
_Parallax_serial_terminal_dat_
'-' 
'-' '***********************************
'-' '* Assembly language serial driver *
'-' '***********************************
'-' 
'-'                         org
'-' '
'-' '
'-' ' Entry
'-' '
'-' entry                   mov     t1,par                'get structure address
	byte	$f0, $a9, $bc, $a0
'-'                         add     t1,#4 << 2            'skip past heads and tails
	byte	$10, $a8, $fc, $80
'-' 
'-'                         rdlong  t2,t1                 'get rx_pin
	byte	$54, $aa, $bc, $08
'-'                         mov     rxmask,#1
	byte	$01, $b2, $fc, $a0
'-'                         shl     rxmask,t2
	byte	$55, $b2, $bc, $2c
'-' 
'-'                         add     t1,#4                 'get tx_pin
	byte	$04, $a8, $fc, $80
'-'                         rdlong  t2,t1
	byte	$54, $aa, $bc, $08
'-'                         mov     txmask,#1
	byte	$01, $be, $fc, $a0
'-'                         shl     txmask,t2
	byte	$55, $be, $bc, $2c
'-' 
'-'                         add     t1,#4                 'get rxtx_mode
	byte	$04, $a8, $fc, $80
'-'                         rdlong  rxtxmode,t1
	byte	$54, $ae, $bc, $08
'-' 
'-'                         add     t1,#4                 'get bit_ticks
	byte	$04, $a8, $fc, $80
'-'                         rdlong  bitticks,t1
	byte	$54, $b0, $bc, $08
'-' 
'-'                         add     t1,#4                 'get buffer_ptr
	byte	$04, $a8, $fc, $80
'-'                         rdlong  rxbuff,t1
	byte	$54, $b4, $bc, $08
'-'                         mov     txbuff,rxbuff
	byte	$5a, $c0, $bc, $a0
'-'                         add     txbuff,#BUFFER_LENGTH
	byte	$40, $c0, $fc, $80
'-' 
'-'                         test    rxtxmode,#%100  wz    'init tx pin according to mode
	byte	$04, $ae, $7c, $62
'-'                         test    rxtxmode,#%010  wc
	byte	$02, $ae, $7c, $61
'-'         if_z_ne_c       or      outa,txmask
	byte	$5f, $e8, $9b, $68
'-'         if_z            or      dira,txmask
	byte	$5f, $ec, $ab, $68
'-' 
'-'                         mov     txcode,#transmit      'initialize ping-pong multitasking
	byte	$33, $c8, $fc, $a0
'-' '
'-' '
'-' ' Receive
'-' '
'-' receive                 jmpret  rxcode,txcode         'run chunk of tx code, then return
	byte	$64, $bc, $bc, $5c
'-' 
'-'                         test    rxtxmode,#%001  wz    'wait for start bit on rx pin
	byte	$01, $ae, $7c, $62
'-'                         test    rxmask,ina      wc
	byte	$f2, $b3, $3c, $61
'-'         if_z_eq_c       jmp     #receive
	byte	$16, $00, $64, $5c
'-' 
'-'                         mov     rxbits,#9             'ready to receive byte
	byte	$09, $b8, $fc, $a0
'-'                         mov     rxcnt,bitticks
	byte	$58, $ba, $bc, $a0
'-'                         shr     rxcnt,#1
	byte	$01, $ba, $fc, $28
'-'                         add     rxcnt,cnt                          
	byte	$f1, $bb, $bc, $80
'-' 
'-' :bit                    add     rxcnt,bitticks        'ready next bit period
	byte	$58, $ba, $bc, $80
'-' 
'-' :wait                   jmpret  rxcode,txcode         'run chunk of tx code, then return
	byte	$64, $bc, $bc, $5c
'-' 
'-'                         mov     t1,rxcnt              'check if bit receive period done
	byte	$5d, $a8, $bc, $a0
'-'                         sub     t1,cnt
	byte	$f1, $a9, $bc, $84
'-'                         cmps    t1,#0           wc
	byte	$00, $a8, $7c, $c1
'-'         if_nc           jmp     #:wait
	byte	$1f, $00, $4c, $5c
'-' 
'-'                         test    rxmask,ina      wc    'receive bit on rx pin
	byte	$f2, $b3, $3c, $61
'-'                         rcr     rxdata,#1
	byte	$01, $b6, $fc, $30
'-'                         djnz    rxbits,#:bit
	byte	$1e, $b8, $fc, $e4
'-' 
'-'                         shr     rxdata,#32-9          'justify and trim received byte
	byte	$17, $b6, $fc, $28
'-'                         and     rxdata,#$FF
	byte	$ff, $b6, $fc, $60
'-'                         test    rxtxmode,#%001  wz    'if rx inverted, invert byte
	byte	$01, $ae, $7c, $62
'-'         if_nz           xor     rxdata,#$FF
	byte	$ff, $b6, $d4, $6c
'-' 
'-'                         rdlong  t2,par                'save received byte and inc head
	byte	$f0, $ab, $bc, $08
'-'                         add     t2,rxbuff
	byte	$5a, $aa, $bc, $80
'-'                         wrbyte  rxdata,t2
	byte	$55, $b6, $3c, $00
'-'                         sub     t2,rxbuff
	byte	$5a, $aa, $bc, $84
'-'                         add     t2,#1
	byte	$01, $aa, $fc, $80
'-'                         and     t2,#BUFFER_MASK
	byte	$3f, $aa, $fc, $60
'-'                         wrlong  t2,par
	byte	$f0, $ab, $3c, $08
'-' 
'-'                         jmp     #receive              'byte done, receive next byte
	byte	$16, $00, $7c, $5c
'-' '
'-' '
'-' ' Transmit
'-' '
'-' transmit                jmpret  txcode,rxcode         'run chunk of rx code, then return
	byte	$5e, $c8, $bc, $5c
'-' 
'-'                         mov     t1,par                'check for head <> tail
	byte	$f0, $a9, $bc, $a0
'-'                         add     t1,#2 << 2
	byte	$08, $a8, $fc, $80
'-'                         rdlong  t2,t1
	byte	$54, $aa, $bc, $08
'-'                         add     t1,#1 << 2
	byte	$04, $a8, $fc, $80
'-'                         rdlong  t3,t1
	byte	$54, $ac, $bc, $08
'-'                         cmp     t2,t3           wz
	byte	$56, $aa, $3c, $86
'-'         if_z            jmp     #transmit
	byte	$33, $00, $68, $5c
'-' 
'-'                         add     t3,txbuff             'get byte and inc tail
	byte	$60, $ac, $bc, $80
'-'                         rdbyte  txdata,t3
	byte	$56, $c2, $bc, $00
'-'                         sub     t3,txbuff
	byte	$60, $ac, $bc, $84
'-'                         add     t3,#1
	byte	$01, $ac, $fc, $80
'-'                         and     t3,#BUFFER_MASK
	byte	$3f, $ac, $fc, $60
'-'                         wrlong  t3,t1
	byte	$54, $ac, $3c, $08
'-' 
'-'                         or      txdata,#$100          'ready byte to transmit
	byte	$00, $c3, $fc, $68
'-'                         shl     txdata,#2
	byte	$02, $c2, $fc, $2c
'-'                         or      txdata,#1
	byte	$01, $c2, $fc, $68
'-'                         mov     txbits,#11
	byte	$0b, $c4, $fc, $a0
'-'                         mov     txcnt,cnt
	byte	$f1, $c7, $bc, $a0
'-' 
'-' :bit                    test    rxtxmode,#%100  wz    'output bit on tx pin 
	byte	$04, $ae, $7c, $62
'-'                         test    rxtxmode,#%010  wc    'according to mode
	byte	$02, $ae, $7c, $61
'-'         if_z_and_c      xor     txdata,#1
	byte	$01, $c2, $e0, $6c
'-'                         shr     txdata,#1       wc
	byte	$01, $c2, $fc, $29
'-'         if_z            muxc    outa,txmask        
	byte	$5f, $e8, $ab, $70
'-'         if_nz           muxnc   dira,txmask
	byte	$5f, $ec, $97, $74
'-'                         add     txcnt,bitticks        'ready next cnt
	byte	$58, $c6, $bc, $80
'-' 
'-' :wait                   jmpret  txcode,rxcode         'run chunk of rx code, then return
	byte	$5e, $c8, $bc, $5c
'-' 
'-'                         mov     t1,txcnt              'check if bit transmit period done
	byte	$63, $a8, $bc, $a0
'-'                         sub     t1,cnt
	byte	$f1, $a9, $bc, $84
'-'                         cmps    t1,#0           wc
	byte	$00, $a8, $7c, $c1
'-'         if_nc           jmp     #:wait
	byte	$4d, $00, $4c, $5c
'-' 
'-'                         djnz    txbits,#:bit          'another bit to transmit?
	byte	$46, $c4, $fc, $e4
'-' 
'-'                         jmp     #transmit             'byte done, transmit next byte
	byte	$33, $00, $7c, $5c
'-' '
'-' '
'-' ' Uninitialized data
'-' '
'-' t1                      res     1
'-' t2                      res     1
'-' t3                      res     1
'-' 
'-' rxtxmode                res     1
'-' bitticks                res     1
'-' 
'-' rxmask                  res     1
'-' rxbuff                  res     1
'-' rxdata                  res     1
'-' rxbits                  res     1
'-' rxcnt                   res     1
'-' rxcode                  res     1
'-' 
'-' txmask                  res     1
'-' txbuff                  res     1
'-' txdata                  res     1
'-' txbits                  res     1
'-' txcnt                   res     1
'-' txcode                  res     1
	byte	$30, $00, $00, $00, $31, $00, $00, $00, $32, $00, $00, $00, $33, $00, $00, $00
	byte	$34, $00, $00, $00, $35, $00, $00, $00, $36, $00, $00, $00, $37, $00, $00, $00
	byte	$38, $00, $00, $00, $39, $00, $00, $00, $41, $00, $00, $00, $42, $00, $00, $00
	byte	$43, $00, $00, $00, $44, $00, $00, $00, $45, $00, $00, $00, $46, $00, $00, $00
	long
_parallel_dat_
'-'                 org 0
'-' 
'-' 
'-' entry
'-'                 mov         t1,par              ' get the adress of the structure that contain all info
	byte	$f0, $a5, $bc, $a0
'-'                 xor         $,$ WC              ' clear c flag
	byte	$01, $02, $bc, $6d
'-'                 muxc        outa, INIT_OUT_L    ' make sure all output that start low state are low
	byte	$4e, $e8, $bf, $70
'-'                 muxnc       outa, INIT_OUT_H    ' this set all pins to high that need to be high
	byte	$4f, $e8, $bf, $74
'-'                 muxc        dira, INIT_DIR_IN   ' all dir input pin set to low
	byte	$51, $ec, $bf, $70
'-'                 muxnc       dira, INIT_DIR_OUT  ' this make port as output
	byte	$50, $ec, $bf, $74
'-' 
'-' ' Basically we use Data in Locks to tell cog we have something to do
'-' ' and Data out lock to send messages back
'-'                 rdlong      rdvalid,t1       ' get read lock
	byte	$52, $a6, $bc, $08
'-'                 add         t1,#1           ' get lock id
	byte	$01, $a4, $fc, $80
'-'                 rdlong      wrvalid,t1       ' get write lock
	byte	$52, $ac, $bc, $08
'-' 
'-' theloop
'-'                 call        #waitlrd            ' get function
	byte	$0e, $2e, $fc, $5c
'-'                 mov         wrcode,rdcode
	byte	$54, $ae, $bc, $a0
'-'                 mov         wrdata,rddata
	byte	$55, $b0, $bc, $a0
'-'                 call        #waitlwr
	byte	$18, $40, $fc, $5c
'-'                 jmp         theloop
	byte	$09, $00, $3c, $5c
'-' 
'-' 
'-' ' wait for byte read to become valid
'-' ' returns with rdcode and rddata to work with
'-' waitlrd
'-'                 rdlong      rdcode,rdvalid     wz
	byte	$53, $a8, $bc, $0a
'-'         IF_Z    jmp         #waitlrd
	byte	$0e, $00, $68, $5c
'-'                 test        rdcode,VALID_BIT   wz
	byte	$37, $a8, $3c, $62
'-'         IF_Z    jmp         #waitlrd
	byte	$0e, $00, $68, $5c
'-'                 mov         t1,par
	byte	$f0, $a5, $bc, $a0
'-'                 add         t1,#RD_ADLDATA          ' point to rdcode
	byte	$08, $a4, $fc, $80
'-'                 rdlong      rddata,t1               ' get code data
	byte	$52, $aa, $bc, $08
'-'                 andn        rdcode,VALID_BIT   wr   ' clear that bit
	byte	$37, $a8, $bc, $64
'-'                 wrlong      rdcode,rdvalid          ' the hub now knows we have read data   
	byte	$53, $a8, $3c, $08
'-' waitlrd_ret     ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' wait for write lock to clear
'-' ' send the wrcode and wrdata back to the hub
'-' waitlwr
'-'                 rdlong      valid,wrvalid       wz  ' must test to see data has been read
	byte	$56, $b2, $bc, $0a
'-'         IF_NZ   test        valid,VALID_BIT     wz  ' test that the valid bit is still set
	byte	$37, $b2, $14, $62
'-'         IF_NZ   jmp         #waitlwr
	byte	$18, $00, $54, $5c
'-'                 or          wrcode,VALID_BIT    wr  ' make sure valid bit is set
	byte	$37, $ae, $bc, $68
'-'                 mov         t1,par
	byte	$f0, $a5, $bc, $a0
'-'                 add         t1,#WR_ADLDATA          ' pont to code
	byte	$0c, $a4, $fc, $80
'-'                 wrlong      wrdata,t1               ' save to hub
	byte	$52, $b0, $3c, $08
'-'                 wrlong      wrcode,wrvalid          'now send code 
	byte	$56, $ae, $3c, $08
'-' waitlwr_ret     ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' Turn the 8 bit buffer to send data 
'-' U2_Enable
'-'                 xor         $,$ wc          ' clear c
	byte	$21, $42, $bc, $6d
'-'                 muxc        outa,U2_OE_PIN  ' this should low to enable buffer
	byte	$38, $e8, $bf, $70
'-' U2_Enable_ret   ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' disable 8 bit buffer
'-' U2_Disable
'-'                 xor         $,$ wc          ' clear c
	byte	$24, $48, $bc, $6d
'-'                 muxnc       outa,U2_OE_PIN  ' this pin should be hi disable buffer
	byte	$38, $e8, $bf, $74
'-' U2_Disable_ret  ret
	byte	$00, $00, $7c, $5c
'-' 
'-' ' set direction of data for u2
'-' U2_Input
'-'                 xor         $,$ wc          'clear c
	byte	$27, $4e, $bc, $6d
'-'                 muxnc       outa,U2_DIR_PIN
	byte	$39, $e8, $bf, $74
'-' U2_Input_ret    ret
	byte	$00, $00, $7c, $5c
'-' 
'-' U2_Output       xor         $,$ wc
	byte	$2a, $54, $bc, $6d
'-'                 muxc        outa,U2_DIR_PIN
	byte	$39, $e8, $bf, $70
'-' U2_Output_ret   ret
	byte	$00, $00, $7c, $5c
'-' 
'-' Strobe          xor         $,$ wc
	byte	$2d, $5a, $bc, $6d
'-'                 muxc        outa,U3_STROBE_PIN
	byte	$3d, $e8, $bf, $70
'-'                 muxnc       outa,U3_STROBE_PIN
	byte	$3d, $e8, $bf, $74
'-'                 muxc        outa,U3_STROBE_PIN
	byte	$3d, $e8, $bf, $70
'-' Strobe_ret      ret
	byte	$00, $00, $7c, $5c
'-' 
'-' LineFeed        xor         $,$ wc
	byte	$32, $64, $bc, $6d
'-'                 muxc        outa,U3_LINEFEED_PIN
	byte	$3e, $e8, $bf, $70
'-'                 muxnc       outa,U3_LINEFEED_PIN
	byte	$3e, $e8, $bf, $74
'-'                 muxc        outa,U3_LINEFEED_PIN
	byte	$3e, $e8, $bf, $70
'-' LineFeed_ret    ret
	byte	$00, $00, $7c, $5c
'-' 
'-' 
'-' VALID_BIT       long    |< 31
	byte	$00, $00, $00, $80
'-' U2_OE_PIN       long    |< 17       ' OE pin
	byte	$00, $00, $02, $00
'-' U2_DIR_PIN      long    |< 23
	byte	$00, $00, $80, $00
'-' U2_DATA_PIN     long    $FF         ' set out latch with FF hex
	byte	$ff, $00, $00, $00
'-' U2_INIT         long    U2_DATA_PIN | U2_DIR_PIN | U2_OE_PIN
	byte	$3b, $00, $00, $00
'-' 
'-' U3_DIR_PIN      long    |< 18
	byte	$00, $00, $04, $00
'-' U3_STROBE_PIN   long    |< 8
	byte	$00, $01, $00, $00
'-' U3_LINEFEED_PIN long    |< 9
	byte	$00, $02, $00, $00
'-' U3_INIT         long    U3_DIR_PIN | U3_STROBE_PIN | U3_LINEFEED_PIN
	byte	$3f, $00, $00, $00
'-' 
'-' U4_DIR_PIN      long    |< 19
	byte	$00, $00, $08, $00
'-' U4_INIT_PIN     long    |< 10
	byte	$00, $04, $00, $00
'-' U4_SELIN_PIN    long    |< 11
	byte	$00, $08, $00, $00
'-' U4_INIT         long    U4_DIR_PIN | U4_INIT_PIN | U4_SELIN_PIN
	byte	$43, $00, $00, $00
'-' 
'-' U5_DIR_PIN      long    |< 20
	byte	$00, $00, $10, $00
'-' U5_BUSY_PIN     long    |< 13
	byte	$00, $20, $00, $00
'-' U5_ACK_PIN      long    |< 12
	byte	$00, $10, $00, $00
'-' U5_INIT         long    U5_ACK_PIN | U5_BUSY_PIN
	byte	$47, $00, $00, $00
'-' 
'-' U6_DIR_PIN      long    |< 21
	byte	$00, $00, $20, $00
'-' U6_SELECT_PIN   long    |< 15
	byte	$00, $80, $00, $00
'-' U6_PEND_PIN     long    |< 14
	byte	$00, $40, $00, $00
'-' U6_INIT         long    U6_SELECT_PIN | U6_PEND_PIN
	byte	$4b, $00, $00, $00
'-' 
'-' U7_DIR_PIN      long    |< 22
	byte	$00, $00, $40, $00
'-' U7_ERROR_PIN    long    |< 16
	byte	$00, $00, $01, $00
'-' 
'-' INIT_OUT_L      long    U2_DIR_PIN | U3_DIR_PIN | U4_DIR_PIN
	byte	$7d, $00, $00, $00
'-' INIT_OUT_H      long    U5_DIR_PIN | U6_DIR_PIN | U7_DIR_PIN
	byte	$4c, $00, $00, $00
'-' INIT_DIR_OUT    long    U2_INIT | U3_INIT | U4_INIT | U5_DIR_PIN | U6_DIR_PIN | U7_DIR_PIN
	byte	$7f, $00, $00, $00
'-' INIT_DIR_IN     long    U5_INIT | U6_INIT | U7_ERROR_PIN
	byte	$4f, $00, $00, $00
objmem
	long	0[714]
stackspace
	long	0[1]
	org	COG_BSS_START
_var01
	res	1
_var02
	res	1
_var03
	res	1
_var04
	res	1
_var05
	res	1
_var06
	res	1
_var07
	res	1
_var08
	res	1
_var09
	res	1
arg01
	res	1
arg02
	res	1
arg03
	res	1
arg04
	res	1
local01
	res	1
local02
	res	1
local03
	res	1
local04
	res	1
local05
	res	1
local06
	res	1
muldiva_
	res	1
muldivb_
	res	1
LMM_RETREG
	res	1
LMM_FCACHE_START
	res	97
LMM_FCACHE_END
	fit	496
